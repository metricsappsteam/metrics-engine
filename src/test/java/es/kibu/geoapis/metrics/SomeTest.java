package es.kibu.geoapis.metrics;

import es.kibu.geoapis.metrics.engine.evaluation.DependencyExtractionVisitor;
import es.kibu.geoapis.metrics.nashorn.MetricsSparkJs;
import es.kibu.geoapis.metrics.utils.Utils;
import jdk.nashorn.internal.ir.FunctionNode;
import jdk.nashorn.internal.parser.Parser;
import jdk.nashorn.internal.runtime.Context;
import jdk.nashorn.internal.runtime.ErrorManager;
import jdk.nashorn.internal.runtime.Source;
import jdk.nashorn.internal.runtime.options.Options;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by lrodr_000 on 24/08/2016.
 */
public class SomeTest {
    Logger logger = LoggerFactory.getLogger(SomeTest.class);


    private static String getFileContent(Object app, String fileName) {
        ClassLoader classLoader = app.getClass().getClassLoader();
        return Utils.getFileContent(fileName, classLoader)/*getFileContent()*/;

    }

    public void testSimpleScriptNoContext() {
        App app = new App();
        String javascript = getFileContent(app, "scripts/word_count.js");
        Object result = executeJavaScript(javascript, null);
        logger.debug("--->> testSimpleScriptNoContext <<---");
        logger.debug(String.format("result :>> %s", result));

    }

    public static Object executeJavaScript(String javascript, JavaSparkContext context) {

        MetricsSparkJs js = new MetricsSparkJs(context);
        js.setSparkContextVarName("sparkContext");
        Object ret;
        try {
            ret = js.eval(javascript);
        } catch (Exception e) {
            System.err.println(e);
            ret = e;
        }
        return ret;
    }

    public static void main(String[] args) {
        SomeTest appTest = new SomeTest();
        appTest.testSimpleScriptNoContext();
    }

    @Test
    public void nashornParsingTest() {

        Options options = new Options("nashorn");
        options.set("anon.functions", true);
        options.set("parse.only", true);
        options.set("scripting", true);

        ErrorManager errors = new ErrorManager();
        Context context = new Context(options, errors, Thread.currentThread().getContextClassLoader());


        String sourceCode = "var a = 10; var b = a + 1; var fn = function () { return a + b}; " +
                "function someFunction() { var b = 0 ; return b + 1; }  ";

        Source source = Source.sourceFor("metricsScript", sourceCode);

        Parser parser = new Parser(context.getEnv(), source, errors);
        FunctionNode functionNode = parser.parse();

        DependencyExtractionVisitor visitor = new DependencyExtractionVisitor();
        visitor.visit(functionNode);
        logger.debug(visitor.toString());
    }


}
