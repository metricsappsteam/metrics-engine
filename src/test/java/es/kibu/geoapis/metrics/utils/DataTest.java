package es.kibu.geoapis.metrics.utils;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.kibu.geoapis.data.DataCreator;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.data.VariableDataGenerator;
import es.kibu.geoapis.data.providers.components.DaggerRandomGeneratorMaker;
import es.kibu.geoapis.data.providers.components.DaggerSimpleGeneratorMaker;
import es.kibu.geoapis.data.providers.components.GeneratorsMaker;
import es.kibu.geoapis.metrics.TestUtils;
import es.kibu.geoapis.metrics.engine.MetricUtils;
import es.kibu.geoapis.objectmodel.Dimension;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;
import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by lrodr_000 on 19/09/2016.
 */
public class DataTest {

    Logger logger = LoggerFactory.getLogger(DataTest.class);

    //GeneratorsMaker generatorsMaker = DaggerSimpleGeneratorMaker.create();
    public static final String GENERATED_DATA_DIR = "generated-data";

    @Test
    public void generateDataTest() throws Exception {

        String metricDefinitionFile = "sample-metrics/sample-metric-real-filters-scopes.json";
        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition(metricDefinitionFile);

        List<String> parquetFileNames = generateTestData(metricsDefinition);

        logger.debug("Parquet files: {}", parquetFileNames);

    }

    private List<String> generateTestData(MetricsDefinition metricsDefinition) throws Exception {
        GeneratorsMaker randomMaker = DaggerRandomGeneratorMaker.create();

        DataCreator dataGenerator = randomMaker.generator();

        DateTime now = DateTime.now();

        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy_MM_dd_HH_mm_ss");
        String nowStr = fmt.print(now);

        File newDir = new File(FileUtils.getTempDirectoryPath() + "test_" + nowStr);

        List<String> parquetFileNames = new ArrayList<>();

        if (newDir.mkdir()) {
            String tempDirectoryPath = newDir.getAbsolutePath();
            List<String> files = DataUtils.generateToFile(tempDirectoryPath, "variable", metricsDefinition, 500, dataGenerator);

            logger.debug("Files {}", files);

            for (String file : files) {
                File realfile = new File(file);
                String jsonFile = realfile.getAbsolutePath();
                String parquetFileName = GENERATED_DATA_DIR + File.separator + nowStr + File.separator+ realfile.getName()+".parquet";
                writeToParquet(jsonFile, parquetFileName);
                parquetFileNames.add(parquetFileName);
            }

        }
        return parquetFileNames;
    }

    public void writeToParquet(String jsonFile, String parquetFileName ) throws Exception {

        ScriptEngine engine = TestUtils.getNewEngine();
        /*var generate = function(jsonFile, parquetFileName)*/

        TestUtils.evalJSResource(engine, "/scripts/parquetGenerator.js");
        Object ret = ((Invocable) engine).invokeFunction("generate", jsonFile, parquetFileName);

    }



}