package es.kibu.geoapis.metrics.engine;

import es.kibu.geoapis.metrics.MetricUtils;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.Assert.assertTrue;

/**
 * Created by lrodr_000 on 12/09/2016.
 */
public class DefaultStatefulMetricsEngineTest {

    Logger logger = LoggerFactory.getLogger(DefaultStatefulMetricsEngineTest.class);

    @Test
    public void getScript() throws Exception {

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-simple-spark.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine();

        try {
            engine.init(metricsDefinition);
            String script = engine.getScript();
            logger.debug(script);
            assertTrue(!script.isEmpty());
        } finally {
            engine.stop();
        }
    }

    @Test
    public void eval() throws Exception {

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-simple-spark.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine(true);

        try {
            engine.init(metricsDefinition);

            String script = engine.getScript();
            logger.debug(script);

            Metric simpleMetric = metricsDefinition.getMetricByName("simpleMetric");

            Object result = engine.eval(simpleMetric);

            MetricUtils.dumpResult(result);

        } finally {
            engine.stop();
        }
    }

    /*This text will prove that is possible to register the metric context*/
    @Test
    public void testEvalContextRegisteredValues() throws Exception {

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-simple-spark-context-test.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine(true);

        //app
        String applicationId = "applicationId";
        String applicationIdValue = "sample-application-id";

        //user
        String userId = "userId";
        String userIdValue = "sample-user-id";

        //session
        String sessionId = "sessionId";
        String sessionIdValue = "sample-session-id";

        engine.registerInMetricsContext(applicationId, applicationIdValue);
        engine.registerInMetricsContext(userId, userIdValue);
        engine.registerInMetricsContext(sessionId, sessionIdValue);

        try {
            engine.init(metricsDefinition);
            String script = engine.getScript();
            logger.debug(script);

            evalAndCheckMetric(metricsDefinition, engine, applicationId, applicationIdValue);
            evalAndCheckMetric(metricsDefinition, engine, userId, userIdValue);
            evalAndCheckMetric(metricsDefinition, engine, sessionId, sessionIdValue);


        } finally {
            engine.stop();
        }

    }

    @Test
    public void testContextDynamicallyAdded() throws Exception {

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-simple-spark-context-test.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine(true);

        //app
        String applicationId = "applicationId";
        String applicationIdValue = "sample-application-id";

        //user
        String userId = "userId";
        String userIdValue = "sample-user-id";

        //session
        String sessionId = "sessionId";
        String sessionIdValue = "sample-session-id";

        engine.registerInMetricsContext(applicationId, applicationIdValue);
        //engine.registerInMetricsContext(userId, userIdValue);
        engine.registerInMetricsContext(sessionId, sessionIdValue);

        try {
            engine.init(metricsDefinition);
            String script = engine.getScript();
            logger.debug(script);

            evalAndCheckMetric(metricsDefinition, engine, applicationId, applicationIdValue);
            evalAndCheckMetric(metricsDefinition, engine, sessionId, sessionIdValue);

            //can be dynamically added later?
            engine.registerInMetricsContext(userId, userIdValue);
            evalAndCheckMetric(metricsDefinition, engine, userId, userIdValue);


            //canbe modified, which would not recommend

            String newUserIdValue = userIdValue + "-modified";
            engine.registerInMetricsContext(userId, newUserIdValue);
            evalAndCheckMetric(metricsDefinition, engine, userId, newUserIdValue);


        } finally {
            engine.stop();
        }

    }

    @Test
    public void evalEquivalentMetricsMustHaveTheSameResult() throws Exception {

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-simple-spark.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine(true);

        try {
            engine.init(metricsDefinition);

            String script = engine.getScript();
            logger.debug(script);

            Metric simpleMetric = metricsDefinition.getMetricByName("simpleMetric");
            Object result = engine.eval(simpleMetric);


            Metric simpleMetric1 = metricsDefinition.getMetricByName("simpleMetricScopes");
            Object result1 = engine.eval(simpleMetric1);

            Object simpleMetricResult1 = MetricUtils.getResultForMetric("simpleMetric", result);
            Object simpleMetricResult2 = MetricUtils.getResultForMetric("simpleMetricScopes", result1);


            assertTrue(simpleMetricResult1.equals(simpleMetricResult2));

        } finally {
            engine.stop();
        }

    }

    private void evalAndCheckMetric(MetricsDefinition metricsDefinition, StatefulMetricsEngine engine, String metricName, String metricValue) {
        Metric simpleMetric = metricsDefinition.getMetricByName(metricName);
        Object result = engine.eval(simpleMetric);


        Object metricResult = MetricUtils.getResultForMetric(metricName, result);
        assertTrue(metricResult.equals(metricValue));
        logger.debug(String.format("Metric '%s' result: %s", metricName, metricResult.toString()));
    }


}