package es.kibu.geoapis.metrics.engine;

import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.test.TestSupport;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.spi.LoggerFactory;
import org.eclairjs.nashorn.NashornEngineSingleton;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptException;
import java.util.List;
import java.util.Map;

import static es.kibu.geoapis.metrics.engine.DefaultMetricsEngine.METRIC;
import static es.kibu.geoapis.metrics.engine.DefaultMetricsEngine.RENDER_CONFIG;

/**
 * Created by lrodr_000 on 21/08/2016.
 */
public class VelocityRendererTest {

    Logger logger = org.slf4j.LoggerFactory.getLogger(VelocityRendererTest.class);

    @Test
    public void simpleCompile() throws ScriptException {
        String script = "var foo = 1";
        checkCompiles(script);

    }

    @Test
    public void simpleCompileShouldFail() throws ScriptException {
        String script = "var foo = ";
        if (checkCompiles(script, true))
        Assert.fail("The script must not compile");
    }


    @Test
    public void allSampleMetricsShouldRenderProperly() throws Exception {
        List<MetricsDefinition> sampleMetricsDefinitions = TestSupport.getSampleMetrics();

        VelocityRenderer renderer = new VelocityRenderer();

        logger.debug("----------------------------------MetricsLoaded---------------------------------------------------");
        logger.debug(sampleMetricsDefinitions.toString());
        logger.debug("-------------------------------------------------------------------------------------");

        for (MetricsDefinition sampleMetricsDefinition : sampleMetricsDefinitions) {
            renderMetric(renderer, sampleMetricsDefinition);
        }

    }

    private void renderMetric(VelocityRenderer renderer, MetricsDefinition sampleMetricsDefinition) {
        Map<String, Object> bindings = new HashedMap();
        bindings.put(METRIC, sampleMetricsDefinition);
        bindings.put(RENDER_CONFIG, new RenderConfig());
        String script = renderer.render(bindings);

        logger.debug("-------------------------------------------------------------------------------------");
        logger.debug("Rendering a template for a test");
        logger.debug(script);
        logger.debug("-------------------------------------------------------------------------------------");

        try {
            checkCompiles(script);
        } catch (ScriptException e) {
            Assert.fail("Script must compile");
        }
    }

    private void checkCompiles(String script) throws ScriptException{
        checkCompiles(script, false);
    }

    public boolean checkCompiles(String script, boolean passive) throws ScriptException {
        try {
            CompiledScript compiledScript = getCompiledScript(script);

            logger.debug(compiledScript.toString());
            return true;
        } catch (ScriptException e) {
            System.err.println(String.format("Error found at %s %d:%d: %s",e.getFileName(), e.getLineNumber(),
                    e.getColumnNumber() , e.getMessage()) );

            if (!passive) {
                throw  e;
            }
            return false;
        }
    }

    private CompiledScript getCompiledScript(String script) throws ScriptException {
        return ((Compilable) NashornEngineSingleton.getEngine()).compile(script);
    }
}