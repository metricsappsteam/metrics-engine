package es.kibu.geoapis.metrics.engine.evaluation;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.metrics.MetricUtils;
import es.kibu.geoapis.metrics.engine.DefaultMetricsEngine;
import es.kibu.geoapis.metrics.engine.RenderConfig;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import org.jgrapht.DirectedGraph;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by lrodriguez2002cu on 08/09/2016.
 */
public class DependencyAnalyzerTest {


    static Logger logger = LoggerFactory.getLogger(DependencyAnalyzerTest.class);

    public static String loopedFunctionsScript =
            "function fn2() { return fn1(1); }  " +
                    "var fn1 = function() { return fn2();}";

    @Test
    public void ciclesWouldNotScrewItUp() {
        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(loopedFunctionsScript);
        analyzer.analyze();

        DependencyAnalyzer.UsageStats uses = analyzer.uses("fn1", "fn2");
        assertTrue(uses.isUsed());
        assertTrue(analyzer.uses("fn2", "fn1").isUsed());
        //assertTrue(uses.areThereCycles());
    }

    @Test
    public void aVariableWithItSelfSaysYes() {

        String sampleScript =
                "function fn2() { return fn1(1); }  " +
                        "var fn1 = function() { return fn2();}";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        assertTrue(analyzer.uses("fn1", "fn1").isUsed());


    }

    @Test
    public void aVariableThatDontExistsCantBeUsed() {

        String sampleScript =
                "function fn2() { return fn1(1); }  " +
                        "var fn1 = function() { return fn2();}";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();
        assertFalse(analyzer.uses("fn3", "fn1").isUsed());

        //if dont exist none of them, then uses must be false.
        assertFalse(analyzer.uses("fn3", "fn3").isUsed());

    }

    @Test
    public void aVariableThatDontExistsCantBeUsedEvenIfListedInComments() {

        String sampleScript =
                "function fn2() { return fn1(1); }  " +
                        "//function fn3() { return fn1(3); }  " +
                        "var fn1 = function() { return fn2();}";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();
        assertFalse(analyzer.uses("fn3", "fn1").isUsed());

        //if dont exist none of them, then uses must be false.
        assertFalse(analyzer.uses("fn3", "fn3").isUsed());

    }

    @Test
    public void aVariableThatDontExistsCantBeUsedEvenIfListedInComments1() {

        String sampleScript =
                "function fn2() { return fn1(1); }  " +
                        "/*function fn3() { return fn1(3); } */ " +
                        "var fn1 = function() { return fn2();}";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();
        assertFalse(analyzer.uses("fn3", "fn1").isUsed());

        //does function fn3 use fn1?
        assertFalse(analyzer.uses("fn1", "fn3").isUsed());

        //if dont exist none of them, then uses must be false.
        assertFalse(analyzer.uses("fn3", "fn3").isUsed());

    }

    public static final String longLoopScript =
            "var fn2 = function() { return fn3(1); } ; " +
                    "function fn3() { return fn4(1); }  " +
                    "var fn4 = function () { return fn5(1); };  " +
                    "function fn5() { return fn1(1); }  " +
                    "var fn1 = function() { return fn2();} ;";

    @Test
    public void evenLongerCiclesWouldNotScrewItUp() {

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(longLoopScript);
        analyzer.analyze();

        DefaultJSDependencyAnalyzer.UsageStats uses = analyzer.uses("fn1", "fn5");
        logger.debug(uses.toString());
        assertTrue(uses.isUsed());
        DefaultJSDependencyAnalyzer.UsageStats usesLong = analyzer.uses("fn2", "fn1");
        logger.debug(usesLong.toString());
        assertTrue(usesLong.isUsed());

    }

    @Test
    public void stackTest() {
        String sampleScript =
                " var a = 0; " +
                        "var fn2 = function() { return fn3(1); } ; " +
                        "function fn3() { return fn4(1); }  " +
                        "var fn4 = function () { return a; };";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        DefaultJSDependencyAnalyzer.UsageStats uses = analyzer.uses("a", "fn2");
        logger.debug(uses.toString());
        assertTrue(uses.isUsed());
        assertTrue(uses.depth() == 3);

    }

    @Test
    public void stackTestVarious() {
        String sampleScript =
                " var a = 0; " +
                        "var fn2 = function() { return a; } ; " +
                        "function fn3() { return fn4(1); }  " +
                        "var fn4 = function () { return a; };";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        DefaultJSDependencyAnalyzer.UsageStats uses = analyzer.uses("a", "fn2");
        logger.debug(uses.toString());
        assertTrue(uses.isUsed());
        assertTrue(uses.depth() == 1);

        DefaultJSDependencyAnalyzer.UsageStats usesOther = analyzer.uses("a", "fn3");
        logger.debug(usesOther.toString());
        assertTrue(usesOther.isUsed());
        assertTrue(usesOther.depth() == 2);


        DefaultJSDependencyAnalyzer.UsageStats usesOther1 = analyzer.uses("fn4", "fn3");
        logger.debug(usesOther1.toString());
        assertTrue(usesOther1.isUsed());
        assertTrue(usesOther1.depth() == 1);

    }


    @Test
    public void stackTestUnused() {
        String sampleScript =
                " var a = 0; " +
                        "var fn2 = function() { return a; } ; " +
                        "function fn3() { return fn4(1); }  " +
                        "var fn4 = function () { return a; };";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        DefaultJSDependencyAnalyzer.UsageStats uses = analyzer.uses("b", "fn2");
        logger.debug(uses.toString());
        assertFalse(uses.isUsed());
        assertTrue(uses.depth() == 0);

    }


    @Test
    public void dependenciesCanBeCarriedByAssigments() throws Exception {

        String sampleScript =
                "var a = 10; " +

                        "var b = a + 1; " +
                        "var g = b; " +
                        "var c = 5 ;" +
                        "var d = 7 ;" +
                        "var fn = function () { return a + b}; " +
                        "function someFunction() { var b = 0 ; return fn(1); }  " +
                        "function fn2() { return fn1(1); }  " +
                        "var fn1 = function() { return g ;}";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();


        //carried by variables assignments
        assertTrue(analyzer.uses("a", "fn1").isUsed());
        assertTrue(analyzer.uses("a", "someFunction").isUsed());
        assertTrue(analyzer.uses("b", "fn1").isUsed());
        assertFalse(analyzer.uses("c", "fn1").isUsed());

        //assertTrue(analyzer.uses("fn", "someFunction").isUsed());

    }




    @Test
    public void usesAreVaried() throws Exception {

        String sampleScript =
                "var a = 10; " +
                        "var b = a + 1; " +
                        "var c = 5 ;" +
                        "var d = 7 ;" +
                        "var fn = function () { return a + b}; " +
                        "function someFunction() { var b = 0 ; return fn(1); }  " +
                        "function fn2() { return fn1(1); }  " +
                        "var fn1 = function() { return fn() + d ;}";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();


        //between functions
        assertTrue(analyzer.uses("fn", "fn1").isUsed());

        assertTrue(analyzer.uses("fn", "someFunction").isUsed());

        //by transitivity
        assertTrue(analyzer.uses("fn", "fn2").isUsed());

        //positives variables

        assertTrue(analyzer.uses("a", "fn").isUsed());

        assertTrue(analyzer.uses("b", "fn").isUsed());

        /**Note: althougth b is also a local variable, someFn calls fn, which uses b*/
        assertTrue(analyzer.uses("b", "someFunction").isUsed());

        /**Note: althougth b is also a local variable, someFn calls fn, which uses b*/
        assertTrue(analyzer.uses("a", "someFunction").isUsed());

        assertTrue(analyzer.uses("b", "fn1").isUsed());

        //negatives
        assertFalse(analyzer.uses("c", "someFunction").isUsed());

        assertFalse(analyzer.uses("c", "fn").isUsed());

        assertFalse(analyzer.uses("c", "fn1").isUsed());

        assertFalse(analyzer.uses("d", "fn").isUsed());

        assertTrue(analyzer.uses("d", "fn1").isUsed());

        assertFalse(analyzer.uses("d", "someFunction").isUsed());

    }


    @Test
    public void usesShouldFail() throws Exception {

        String sampleScript =
                "var a = 10; " +
                        "var b = a + 1; " +
                        "var c = 5 ;" +
                        "var d = 7 ;" +
                        "var fn = function () { return a + b}; " +
                        "function someFunction() { var b = 0 ; return fn(1); }  " +
                        "function fn2() { return fn1(1); }  " +
                        "var fn1 = function() { return fn() + d ;}";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        //negatives
        DependencyAnalyzer.UsageStats uses = analyzer.uses("c", "someFunction");
        assertFalse(uses.isUsed());

    }

    @Test
    public void realSchemaTest() throws Exception {

        String sampleScript =
                "var application = {}; " +
                "var variable1 = 10; " +
                "application.myvar = variable1;"  +
                "var someScope = application.myvar.filter(function(e) {return e.session === sc.session;}); " +
                "var fn = function () { return someScope.operation();}; ";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();


        //negatives
        DependencyAnalyzer.UsageStats uses = analyzer.uses("variable1", "fn");
        assertTrue(uses.isUsed());

    }

    @Test
    public void realSchemaTestIndirect() throws Exception {

        String sampleScript =
                "var application = {}; " +
                "var variable3 = 10; " +
                "var variable1 = 10; " +
                "var variable2 = variable1.filter(); " +
                "application.myvar = variable2;"  +
                "var someScope = application.myvar.filter(function(e) {return e.session === sc.session;}); " +
                "var fn = function () { return someScope.operation();}; ";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        //positives
        DependencyAnalyzer.UsageStats uses = analyzer.uses("variable1", "fn");

        assertTrue(uses.isUsed());

        //negative
        DependencyAnalyzer.UsageStats dontUses = analyzer.uses("variable3", "fn");
        assertFalse(dontUses.isUsed());

    }

    @Test
    public void realSchemaTestIndirectRendered() throws Exception {

        String sampleScript ="var applicationFile = 'datafile.txt';\n" +
                "//application rdd shoud be filled, maybe a parqet file  or something like that\n" +
                "var application  = sc.textFile(applicationFile).cache();\n" +
                "\n" +
                "\n" +
                "var session = {};\n" +
                "var user = {};\n" +
                "var userSession = {};\n" +
                "\n" +
                "//session\n" +
                "session.movement = movement.filter(function(e){return e.session === mc.sessionId; });\n" +
                "\n" +
                "//user\n" +
                "user.movement = movement.filter(function(e){return (e.application === mc.applicationId && e.user === mc.userId); });\n" +
                "\n" +
                "//user-session\n" +
                "userSession.movement = movement.filter(function(e){return (e.application === mc.applicationId && e.session === mc.sessionId); });\n" +
                "\n" +
                "\n" +
                "//Scopes will be registered in the following way there will be some default scopes:\n" +
                "   var withSteps = session.movement.filter(function (record) { return record.steps > 0; });\n" +
                "\n" +
                "var simpleMetric = function (){\n" +
                "    var Tuple2 = require('eclairjs/Tuple2'); var rdd2 = session.movement.filter(function (record) { return record.steps > 0; }); return rdd2.count();\n" +
                "};\n" +
                "var simpleMetricScopes = function (){\n" +
                "    var Tuple2 = require('eclairjs/Tuple2'); return withSteps.count();\n" +
                "};\n"+
                "var simpleMetricScopes1 = function (){\n" +
                "    var Tuple2 = require('eclairjs/Tuple2'); return simpleMetricsScopes();\n" +
                "};\n";



        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        //positives
        DependencyAnalyzer.UsageStats uses = analyzer.uses("movement", "simpleMetric");
        assertTrue(uses.isUsed());

        uses = analyzer.uses("movement", "simpleMetricScopes");
        assertTrue(uses.isUsed());

        uses = analyzer.uses("movement", "simpleMetricScopes1");
        assertTrue(uses.isUsed());

        /*//negative
        DependencyAnalyzer.UsageStats dontUses = analyzer.uses("variable3", "fn");
        assertFalse(dontUses.isUsed());*/

    }

    @Test
    public void usesIndirectlyByCall() throws Exception {

        String sampleScript =
                "var a = 10; " +
                        "var b = a + 1; " +
                        "var fn = function () { return a + b}; " +
                        "function someFunction() { var b = 0 ; return fn(1); }  " +
                        "var fn1 = function() { fn()}";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        boolean usesVariableBIndirectly = analyzer.uses("b", "fn1").isUsed();
        assertTrue(usesVariableBIndirectly == true);
    }

    @Test
    public void usesIndirectlyByDeclaration() throws Exception {

        String sampleScript =
                "var a = 10; " +
                        "var b = a + 1; " +
                        "var fn = function () { return a + b}; " +
                        "function someFunction() { var b = 0 ; return fn(1); }  " +
                        "var fn1 = function() { function fn3() { return b;} };";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        boolean usesVariableBIndirectly = analyzer.uses("b", "fn1").isUsed();
        assertTrue(usesVariableBIndirectly == true);
    }


    @Test
    public void usesIndirectlyByDeclaration1() throws Exception {

        String sampleScript =
                "var a = 10; " +
                        "var b = { " +
                        "\"a\": 1}; " +

                        "var fn = function () { return b.a}; " +
                        "var fn2 = function () { return a}; " +
                        "function someFunction() { var b = 0 ; return fn(1); }  " +
                        "var fn1 = function() { function fn3() { return b;} };";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        boolean usesVariableBIndirectly = analyzer.uses("b", "fn").isUsed();
        boolean usesVariableAIndirectly = analyzer.uses("a", "fn").isUsed();
        boolean usesVariableAIndirectlyFn2 = analyzer.uses("a", "fn2").isUsed();

        assertTrue(usesVariableBIndirectly);
        assertFalse(usesVariableAIndirectly);
        assertTrue(usesVariableAIndirectlyFn2);
    }


    @Test
    public void usesIndirectlyByDeclaration2() throws Exception {

        String sampleScript =
                "var a = 10; " +
                        "var b = 1 ;" +
                        "b = a.filter; " +
                        "var fn = function () { return b}; " +
                        "var fn2 = function () { return a}; " +
                        "function someFunction() { var b = 0 ; return fn(1); }  " +
                        "var fn1 = function() { function fn3() { return b;} };";

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(sampleScript);
        analyzer.analyze();

        //boolean usesVariableBIndirectly = analyzer.uses("b", "fn").isUsed();
        DependencyAnalyzer.UsageStats uses = analyzer.uses("a", "fn");
        boolean usesVariableAIndirectly = uses.isUsed();
        //boolean usesVariableBAIndirectly = analyzer.uses("b", "a").isUsed();
        //boolean usesVariableAIndirectlyFn2 = analyzer.uses("a", "fn2").isUsed();

        /*assertTrue(usesVariableBAIndirectly);
        assertTrue(usesVariableBIndirectly);*/
        assertTrue(usesVariableAIndirectly);
        /*assertTrue(usesVariableAIndirectlyFn2);*/
    }


    String graphScript =
            "var a = 10; " +
                    "var b = a + 1; " +
                    "var fn = function () { return a + b}; " +
                    "function someFunction() { var b = 0 ; return fn(1); }  " +
                    "var fn1 = function() { function fn3() { return b;} };";

    @Test
    public void testGenerateUsageGraphPreconds() throws Exception {

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(graphScript);
        analyzer.analyze();

        assertTrue(analyzer.uses("b", "fn1").isUsed());
        assertTrue(analyzer.uses("b", "fn").isUsed());
        assertTrue(analyzer.uses("a", "fn").isUsed());
    }

    @Test
    public void testGenerateUsageGraph() throws Exception {

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(graphScript);
        analyzer.analyze();

        List<String> variables = new ArrayList<>();
        variables.add("b");
        variables.add("fn1");
        variables.add("a");
        variables.add("fn");
        variables.add("someFunction");

        DirectedGraph<String, DefaultJSDependencyAnalyzer.UsageStats> graph = analyzer.buildUsageGraphFor(variables);

        assertTrue(graph.containsEdge("b", "fn1"));

        assertTrue(graph.containsEdge("b", "fn"));

        //this changed! Under the new algorithm,  as b which is used by fn1 uses a.
        assertTrue(graph.containsEdge("a", "fn1"));

    }

    @Test
    public void testWithRealMetricFileTesting() throws ProcessingException, IOException {

        DefaultMetricsEngine metricsEngine = new DefaultMetricsEngine(true);

        String fileName = "sample-documents/sample-metric.json";

        MetricsDefinition metricsDefinition = withMetricDefinition(fileName);
        String code = "var Tuple2 = require('eclairjs/Tuple2'); \n" +
                "\n" +
                "print('app:' + application.current());" +
                "return application.current();";

        String script = metricsEngine.renderScript(metricsDefinition);

        logger.debug(script);

    }

    @Test
    public void testWithRealMetricFile() throws ProcessingException, IOException {

        DefaultMetricsEngine metricsEngine = new DefaultMetricsEngine();

        String fileName = "sample-documents/sample-metric.json";

        MetricsDefinition metricsDefinition = withMetricDefinition(fileName);

        String code = "var Tuple2 = require('eclairjs/Tuple2'); \n" +
                "\n" +
                "print('app:' + application.current());" +

                //to be removed
                "var rdd0 = application.flatMap(function(sentence) {\n" +

                "    var sp = sentence.split(','); \n" +
                "    print(sentence.split); \n" +
                "    print(sp); \n" +
                "    return sp; \n" +
                "});\n" +
                "textLength.current(); \n" +
                "print('rdd0:' + rdd0.current());" +
                "print('rdd0 collected:' +rdd0.collect());" +
                //to be removed
                "\n" +
                "var turf = require('./lib/turf.min.js');" +

                "var rdd2 = application.flatMap(function(sentence) {\n" +
                "    var sp = sentence.split(','); \n" +
                "    print(sentence.split); \n" +
                "    print(sp); \n" +
                "    return sp; \n" +
                "});\n" +

                "\n" +
                "print('rdd2:' + rdd2.current());" +
                "print('rdd2 collected:' +JSON.stringify(rdd2.collect()));" +
                "\n" +
                "var rdd3 = rdd2.filter(function(word) {\n" +
                //"    print(word);  \n"+
                "    print(JSON.stringify(word));  \n" +
                "    return word.trim().length > 0;\n" +
                "});\n" +
                "\n" +
                "\n" +
                "var rdd4 = rdd3.mapToPair(function(word, Tuple2) {\n" +
                "    return new Tuple2(word, 1);\n" +
                "}, [Tuple2]);\n" +
                "\n" +
                "\n" +
                "\n" +
                "var rdd5 = rdd4.reduceByKey(function(a, b) {\n" +
                "    return a + b;\n" +
                "});\n" +
                "\n" +
                "\n" +
                "var rdd6 = rdd5.mapToPair(function(tuple, Tuple2) {\n" +
                "    return new Tuple2(tuple._2() + 0.0, tuple._1());\n" +
                "}, [Tuple2]);\n" +
                "\n" +
                "\n" +
                "var rdd7 = rdd6.sortByKey(false);\n" +
                "\n" +
                "\n" +
                "var result = rdd7.take(1);\n" +
                "\n" +
                "return result[0];";

        //this overrides the code
        Metric metric = metricsDefinition.getMetricByName("simpleMetric");
        metric.setCode(code);

        RenderConfig config = new RenderConfig();
        config.setRenderSparkContextCreation(true);

        config.setDataFile("D:\\\\Projects\\\\GeoFriends\\\\GeoFences\\\\metrics-engine\\\\src\\\\main\\\\resources\\\\samples\\\\sample1.txt");
        //config.setDataFile(".\\\\samples\\\\sample.txt");

        String script = metricsEngine.renderScript(metricsDefinition, config);

        DefaultJSDependencyAnalyzer analyzer = new DefaultJSDependencyAnalyzer(script);
        analyzer.analyze();

        logger.debug(analyzer.getFunctionsList().toString());

        List<String> variables = new ArrayList<>();
        variables.add("textLength");
        variables.add("simpleMetric");

        DirectedGraph<String, DefaultJSDependencyAnalyzer.UsageStats> graph = analyzer.buildUsageGraphFor(variables);
        assertTrue(graph.containsEdge("textLength", "simpleMetric"));

        DefaultJSDependencyAnalyzer.UsageStats edge = graph.getEdge("textLength", "simpleMetric");
        assertTrue(edge.isUsed());
        assertTrue(edge.depth() == 1);
        logger.debug(edge.toString());

    }

    private MetricsDefinition withMetricDefinition(String fileName) throws ProcessingException, IOException {
        return MetricUtils.withMetricDefinition(fileName);
    }


}