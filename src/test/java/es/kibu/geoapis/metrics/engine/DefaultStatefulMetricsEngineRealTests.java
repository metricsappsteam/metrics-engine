package es.kibu.geoapis.metrics.engine;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.kibu.geoapis.metrics.MetricUtils;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertTrue;

/**
 * Created by lrodr_000 on 19/09/2016.
 */
public class DefaultStatefulMetricsEngineRealTests {

    Logger logger = LoggerFactory.getLogger(DefaultStatefulMetricsEngineRealTests.class);


    @Test
    public void testWorkWithSampleData() throws Exception {

        MetricsDefinition metricsDefinition = es.kibu.geoapis.metrics.MetricUtils.withMetricDefinition("sample-metrics/sample-metric-real-filters-scopes.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine(true);

        //app
        String applicationId = "applicationId";

        //user
        String userId = "userId";
        //session
        String sessionId = "sessionId";

        boolean preEvaluate = true;
        try {

            ((BaseEngine)engine).getConfig().setDebugging(true);
            BaseEngine.SampleData sampleDataRaw = BaseEngine.createSampleData(metricsDefinition);
            Map<String, List<JsonElement>> sampleData = ((Map<String, List<JsonElement>>) sampleDataRaw.sampleDataObject);
            String variableName = "movement";
            String application = sampleData.get(variableName).get(0).getAsJsonObject().get("application").getAsString();
            String user = sampleData.get(variableName).get(0).getAsJsonObject().get("user").getAsString();
            String session = sampleData.get(variableName).get(0).getAsJsonObject().get("session").getAsJsonObject().get("id").getAsString();

            logger.debug("application: {}", application);
            logger.debug("user: {}", user);
            logger.debug("session: {}", session);

            engine.registerInMetricsContext(applicationId, application);
            engine.registerInMetricsContext(userId, user);
            engine.registerInMetricsContext(sessionId, session);

            ((BaseEngine)engine).setSampleData(sampleDataRaw);

            engine.init(metricsDefinition, preEvaluate);
            String script = engine.getScript();

            logger.debug(script);
            if (!preEvaluate) return;

            //Map<String, List<JsonElement>> sampleData = (Map<String, List<JsonElement>> )((BaseEngine) engine).getSampleData();

            //<editor-fold desc="data schema here">
            /*
            * {
              "movement": [
                {
                  "application": "application--2106151166",
                  "session": {
                    "id": "session-986328635",
                    "startTime": "2016-09-19T16:23:42.053+02:00"
                  },
                  "location": {
                    "latitude": 37.18306,
                    "longitude": -3.60206,
                    "time": "2012-11-08T05:53:47.000Z"
                  },
                  "time": "2016-09-19T16:23:42.059+02:00",
                  "user": "user-212313084",
                  "steps": 966354260
                },
                {
                  "application": "application--2106151166",
                  "session": {
                    "id": "session-986328635",
                    "startTime": "2016-09-19T16:23:42.053+02:00"
                  },
            * */
            //</editor-fold>


            Object value2 = evalAndCheckMetric(metricsDefinition, engine, "userSessionMetric");
            logger.debug("value: {}", value2);

            Object value = evalAndCheckMetric(metricsDefinition, engine, "simpleMetric");
            Object value1 = evalAndCheckMetric(metricsDefinition, engine, "simpleMetricScopes");


            logger.debug("value: {}", value);
            logger.debug("value1: {}", value1);

            assertTrue(value.equals(value1));

        } finally {
            if (preEvaluate) {
                engine.stop();
            }
        }

    }


    /**
     * parquetFileName: generated-data\2016_09_19_18_13_37\variable-movement.parquet
     * jsonFile: C:\Users\LRODR_~1\AppData\Local\Temp\test_2016_09_19_18_13_37\variable-movement
     * [Stage 1:>                                                          (0 + 2) / 2]SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
     * SLF4J: Defaulting to no-operation (NOP) logger implementation
     * SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
     * +-----------------------+----------------------------------------------+-------------------------------------------------+----------+------------------------+---------------+
     * |application            |location                                      |session                                          |steps     |time                    |user           |
     * +-----------------------+----------------------------------------------+-------------------------------------------------+----------+------------------------+---------------+
     * |application--1705372261|[37.18306,-3.60206,2012-11-08T05:53:47.000Z]  |[session-937432643,2016-09-19T18:13:37.548+02:00]|611775087 |2012-11-08T05:53:47.000Z|user-1630913046|
     * |application--1705372261|[37.18321,-3.60178,2012-11-08T05:53:51.000Z]  |[session-937432643,2016-09-19T18:13:37.548+02:00]|983445529 |2012-11-08T05:53:51.000Z|user-1630913046|
     * |application--1705372261|[37.18347,-3.60148,2012-11-08T05:53:56.000Z]  |[session-937432643,2016-09-19T18:13:37.548+02:00]|1507418470|2012-11-08T05:53:56.000Z|user-1630913046|
     * |application--1705372261|[37.184175,-3.600781,2012-11-08T05:54:14.000Z]|[session-937432643,2016-09-19T18:13:37.548+02:00]|1915210665|2012-11-08T05:54:14.000Z|user-1630913046|
     * |application--1705372261|[37.18478,-3.60018,2012-11-08T05:54:43.000Z]  |[session-937432643,2016-09-19T18:13:37.548+02:00]|1597610041|2012-11-08T05:54:43.000Z|user-1630913046|
     * +-----------------------+----------------------------------------------+-------------------------------------------------+----------+------------------------+---------------+
     * only showing top 5 rows
     */


    @Test
    public void testWorkWithParquetSampleData() throws Exception {

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-test-dataframe.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine();
        ((BaseEngine) engine).getConfig().setDataFile("generated-data/2016_09_19_18_13_37/");
        //app
        String applicationId = "applicationId";
        //user
        String userId = "userId";
        //session
        String sessionId = "sessionId";

        String application = "application--1705372261";
        String user = "user-1630913046";
        String session = "session-937432643";

        engine.registerInMetricsContext(applicationId, application);
        engine.registerInMetricsContext(userId, user);
        engine.registerInMetricsContext(sessionId, session);

        try {
            boolean preEvaluate = true;

            engine.init(metricsDefinition, preEvaluate);
            String script = engine.getScript();
            System.out.println(script);

            //String variableName = "movement";
            if (preEvaluate == true) {
                Object value = evalAndCheckMetric(metricsDefinition, engine, "simpleMetric");
                Object value1 = evalAndCheckMetric(metricsDefinition, engine, "simpleMetricScopes");

                assertTrue(value.equals(value1));
            }


        } finally {
            engine.stop();
        }

    }
    @Test
    public void testWorkWithParquetSampleDataStepsStats() throws Exception {

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-metrics/sample-metric-test-dataframe.json");
        StatefulMetricsEngine engine = new DefaultStatefulMetricsEngine();
        ((BaseEngine) engine).getConfig().setDataFile("generated-data/2016_09_19_18_13_37/");
        ((BaseEngine) engine).getConfig().setDebugging(true);
        //app
        String applicationId = "applicationId";
        //user
        String userId = "userId";
        //session
        String sessionId = "sessionId";

        String application = "application--1705372261";
        String user = "user-1630913046";
        String session = "session-937432643";

        engine.registerInMetricsContext(applicationId, application);
        engine.registerInMetricsContext(userId, user);
        engine.registerInMetricsContext(sessionId, session);

        try {
            boolean preEvaluate = true;

            engine.init(metricsDefinition, preEvaluate);
            String script = engine.getScript();
            logger.debug(script);
            //String variableName = "movement";
            if (preEvaluate == true) {
                Object stats = evalAndCheckMetric(metricsDefinition, engine, "stepsStats");
                logger.debug("Stats: {}", stats.toString());
                //assertTrue(value.1equals(stats));
            }


        } finally {
            engine.stop();
        }

    }


    private Object evalAndCheckMetric(MetricsDefinition metricsDefinition, StatefulMetricsEngine engine, String metricName) {
        Metric simpleMetric = metricsDefinition.getMetricByName(metricName);
        Object result = engine.eval(simpleMetric);

        Object metricResult = MetricUtils.getResultForMetric(metricName, result);
        logger.debug(String.format("Metric '%s' result: %s", metricName, metricResult.toString()));
        return metricResult;
        //assertTrue(metricResult.equals(metricValue));
    }

}