package es.kibu.geoapis.metrics;

import es.kibu.geoapis.metrics.nashorn.MetricsSparkJs;
import es.kibu.geoapis.metrics.utils.Utils;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    static Logger logger = LoggerFactory.getLogger(AppTest.class);

    private static String getFileContent(Object app, String fileName) {
        ClassLoader classLoader = app.getClass().getClassLoader();
        return Utils.getFileContent(fileName, classLoader)/*getFileContent()*/;

    }

    @Test
    @Ignore
    public void testSimpleScriptNoContext() {
        App app = new App();
        String javascript = getFileContent(app, "scripts/word_count.js");
        JavaSparkContext ctx = new JavaSparkContext("local[*]", "AppTest app");
        Object result = executeJavaScript(javascript, ctx);
        logger.debug("--->> testSimpleScriptNoContext <<---");
        logger.debug(String.format("result :>> %s", result));

    }

    public static Object executeJavaScript(String javascript, JavaSparkContext context) {

        MetricsSparkJs js = new MetricsSparkJs(context);
        js.setSparkContextVarName("sc");
        Object ret;
        try {
            ret = js.eval(javascript);
        } catch (Exception e) {
            System.err.println(e);
            ret = e;
        }
        return ret;
    }

    public static void main(String[] args) {
        AppTest appTest = new AppTest();
        appTest.testSimpleScriptNoContext();
    }
}
