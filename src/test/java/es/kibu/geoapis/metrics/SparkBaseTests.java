package es.kibu.geoapis.metrics;

import com.holdenkarau.spark.testing.SharedJavaSparkContext;
import es.kibu.geoapis.metrics.nashorn.MetricsSparkJs;
import es.kibu.geoapis.metrics.utils.Utils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by lrodr_000 on 22/08/2016.
 */

public class SparkBaseTests extends SharedJavaSparkContext implements Serializable {

    static Logger logger = LoggerFactory.getLogger(SparkBaseTests.class);

   /* @Test
    public void verifyMapTest() {

        List<Integer> input = Arrays.asList(1, 2);
        JavaRDD<Integer> inputRDD = jsc().parallelize(input);
        JavaRDD<Integer> result = inputRDD.map(
                new Function<Integer, Integer>() {
                    public Integer call(Integer x) {
                        return x * x;
                    }
                });
        assertEquals(input.size(), result.count());
    }
*/
 /*   @Ignore
    public void verifyMapTest1() {
        List<Integer> input = Arrays.asList(1, 2);
        JavaRDD<Integer> inputRDD = jsc().parallelize(input);

        JavaRDD<Integer> result = inputRDD.map(
                new Function<Integer, Integer>() {
                    public Integer call(Integer x) {
                        return x * x;
                    }
                });
        assertEquals(input.size(), result.count());
    }*/


    private static String getFileContent(Object app, String fileName) {
        ClassLoader classLoader = app.getClass().getClassLoader();
        return Utils.getFileContent(fileName, classLoader);

    }

    @Test
    public void testSimpleScript() {
        App app = new App();
        String javascript = getFileContent(app, "scripts/word_count.js");
        logger.debug("------Script-----");
        logger.debug(javascript);
        logger.debug("------Script END-----");


        try {
            Object result = executeJavaScript(javascript, jsc());

            logger.debug("testSimpleScript");
            if (result instanceof Exception) {
                logger.error("Error while testing {}", ((Exception) result).getMessage(), (Throwable)result);
                ((Exception) result).printStackTrace();
                fail(((Exception) result).getMessage());
            }
            else {
                logger.debug("The result is: " + result);
            }
        } catch (Exception e) {
            logger.debug("The result is: {}" , e);
        }
    }

  @Test
    public void testSimpleScriptNoContext() {
        App app = new App();
        String javascript = getFileContent(app, "scripts/word_count.js");
        Object result = executeJavaScript(javascript, jsc());
        logger.debug("--->> testSimpleScriptNoContext <<---");
        //logger.debug(String.format("result :>> %s", result));
     }

    public static Object executeJavaScript(String javascript, JavaSparkContext context) {
        MetricsSparkJs js = new MetricsSparkJs(context);
        js.setSparkContextVarName("sc");
        Object ret;
        try {
            ret = js.eval(javascript);
        } catch (Exception e) {
            logger.error("Error  while executing javascript", e);
            ret = e;
        }
        finally {
             if (context == null) {
                 //the context was created outside
                 js.stopContext();
             }
        }
        return ret;
    }

    /*@AfterClass*/
    /*@After
    public void afterClass(){
        jsc().stop();
    }*/
}