package es.kibu.geoapis.metrics;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.metrics.engine.BaseEngine;
import es.kibu.geoapis.metrics.engine.DefaultMetricsEngine;
import es.kibu.geoapis.metrics.engine.MetricsEngine;
import es.kibu.geoapis.metrics.engine.RenderConfig;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Scope;
import org.eclairjs.nashorn.Utils;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.Tuple2;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import java.io.IOException;
import java.lang.reflect.Array;

import static org.junit.Assert.assertEquals;

/**
 * Created by lrodr_000 on 24/08/2016.
 */
public class EclairBasedTests {

    public static final String EXTERNAL_DATA_DIR = "d:\\Projects\\GeoFriends\\GeoFences\\delete\\";
    static Logger logger = LoggerFactory.getLogger(EclairBasedTests.class);

    public static final String JS_TEST_TURF_JS = "/js/test-turf.js";

    private void printReturnValueDetails(Object result) {
        log(result.getClass().getCanonicalName());
    }

    @Test
    public void wordCountTest() throws Exception {

        ScriptEngine engine = TestUtils.getNewEngine();

        TestUtils.evalJSResource(engine, "/scripts/word_count1.js");
        Object ret = ((Invocable) engine).invokeFunction("WordCount1", "d:\\Projects\\GeoFriends\\GeoFences\\metrics-engine\\src\\main\\resources\\samples\\sample.txt");

        printReturnValueDetails(ret);

        Object o = Utils.jsToJava(ret);


        Object member_1 = ((Tuple2) Array.get(o, 0))._1();
        Object member_2 = ((Tuple2) Array.get(o, 0))._2();
        assertEquals("failure - strings are not equal", member_1, 148.0);
        assertEquals("failure - strings are not equal", member_2, "hello");


        Object member_11 = ((Tuple2) Array.get(o, 1))._1();
        Object member_12 = ((Tuple2) Array.get(o, 1))._2();
        assertEquals("failure - strings are not equal", member_11, 148.0);
        assertEquals("failure - strings are not equal", member_12, "world");

        Object member_21 = ((Tuple2) Array.get(o, 2))._1();
        Object member_22 = ((Tuple2) Array.get(o, 2))._2();
        assertEquals("failure - strings are not equal", member_21, 4.0);
        assertEquals("failure - strings are not equal", member_22, "funny");

    }


    @Test
    public void wordCountTestTurf() throws Exception {
        ScriptEngine engine = TestUtils.getNewEngine();

        try {
            TestUtils.evalJSResource(engine, "/scripts/word_countTurf.js");
            Object ret = ((Invocable) engine).invokeFunction("WordCount1",
                    "d:\\Projects\\GeoFriends\\GeoFences\\metrics-engine\\src\\main\\resources\\samples\\sample.txt");

            printReturnValueDetails(ret);

            Object o = Utils.jsToJava(ret);


            Object member_1 = ((Tuple2) Array.get(o, 0))._1();
            Object member_2 = ((Tuple2) Array.get(o, 0))._2();
            assertEquals("failure - strings are not equal", member_1, 148.0);
            assertEquals("failure - strings are not equal", member_2, "hello");


            Object member_11 = ((Tuple2) Array.get(o, 1))._1();
            Object member_12 = ((Tuple2) Array.get(o, 1))._2();
            assertEquals("failure - strings are not equal", member_11, 148.0);
            assertEquals("failure - strings are not equal", member_12, "world");

            Object member_21 = ((Tuple2) Array.get(o, 2))._1();
            Object member_22 = ((Tuple2) Array.get(o, 2))._2();
            assertEquals("failure - strings are not equal", member_21, 4.0);
            assertEquals("failure - strings are not equal", member_22, "funny");
        } finally {
            ((Invocable) engine).invokeFunction("stop");
        }


    }

    @Test
    @Ignore //ignored because  Turf does not work
    public void turfTest() throws Exception {
        ScriptEngine engine = TestUtils.getNewEngine();

        TestUtils.evalJSResource(engine, JS_TEST_TURF_JS);

        //callFunction((Invocable) engine, "centerFn");

        callFunction((Invocable) engine, "arraysTest");

        //callFunction((Invocable) engine, "simplifyFn");

    }

    private void callFunction(Invocable engine, String fn) throws ScriptException, NoSuchMethodException {

        log(String.format("-------------------------------- '%s'   ------------------------------------", fn));
        log(String.format("calling function '%s'", fn));
        Object ret;
        ret = engine.invokeFunction(fn);
        log(ret);
        log(String.format("end calling function '%s'", fn));
        log("-------------------------------------   -----------------------------------------------");

    }

    private void log(Object message) {
        logger.debug(message.toString());
    }

    @Test
    public void sparkQLTest() throws Exception {
        ScriptEngine engine = TestUtils.getNewEngine();

        TestUtils.evalJSResource(engine, "/scripts/sparkql.js");
        Object ret = ((Invocable) engine).invokeFunction("sparkQL", "d:\\Projects\\GeoFriends\\GeoFences\\metrics-engine\\src\\main\\resources\\samples\\people.txt",
                "d:\\Projects\\GeoFriends\\GeoFences\\metrics-engine\\src\\main\\resources\\samples\\test.json");

    }

    @Test
    public void dataPreparationSparkQLTest() throws Exception {
        ScriptEngine engine = TestUtils.getNewEngine();

        TestUtils.evalJSResource(engine, "/scripts/sparkParquet.js");
        Object ret = ((Invocable) engine).invokeFunction("sparkQL",
                /*"samples/variable-textLength"*/"d:\\Projects\\GeoFriends\\GeoFences\\metrics-engine\\src\\main\\resources\\samples\\variable-textLength");


    }

    @Test
    public void testRealScript() throws Exception {
        ScriptEngine engine = TestUtils.getNewEngine();
        TestUtils.evalJSResource(engine, "/scripts/sample-metric-test-dataframe.json.js");
    }

    @Test
    public void dataPreparationSparkQLTest50K() throws Exception {
        ScriptEngine engine = TestUtils.getNewEngine();
        TestUtils.evalJSResource(engine, "/scripts/sparkParquet.js");
        String filename = EXTERNAL_DATA_DIR + "variable-textLength50K";
        Object ret = ((Invocable) engine).invokeFunction("sparkQL",
                filename);

    }

    @Test
    public void dataPreparationSparkQLTest500K() throws Exception {
        ScriptEngine engine = TestUtils.getNewEngine();
        TestUtils.evalJSResource(engine, "/scripts/sparkParquet.js");
        String filename = EXTERNAL_DATA_DIR + "variable-textLength500K";
        Object ret = ((Invocable) engine).invokeFunction("sparkQL",
                filename);

    }

    @Test
    public void dataPreparationSparkQLTest500KRandom() throws Exception {
        ScriptEngine engine = TestUtils.getNewEngine();
        TestUtils.evalJSResource(engine, "/scripts/sparkParquet.js");
        String filename = EXTERNAL_DATA_DIR + "variable-textLength500KRandom";
        Object ret = ((Invocable) engine).invokeFunction("sparkQL",
                filename);

        logger.debug(String.format("Parquet file:>> %s", ret.toString()));

    }

    @Test
    public void dataReadAndTestSparkQL500KRandom() throws Exception {

        ScriptEngine engine = TestUtils.getNewEngine();

        String filename = EXTERNAL_DATA_DIR + "variable-textLength500KRandom";
        TestUtils.evalJSResource(engine, "/scripts/sparkSessionazedParquet.js");
        String parquetFile = "data.parquet_912";

        Object ret = ((Invocable) engine).invokeFunction("sparkQL",
                filename, parquetFile);

    }


    @Test
    public void sampleMetrics() throws IOException, ProcessingException {

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-documents/sample-metric.json");
        MetricsEngine metricsEngine = new DefaultMetricsEngine();
        //metricsEngine.setTesting(true);

        try {
            //language="JS"
            String code = "var Tuple2 = require('eclairjs/Tuple2'); \n" +
                    "\n" +
                    "print('app:' + application.count());" +

                    //to be removed
                    "var rdd0 = application.flatMap(function(sentence) {\n" +

                    "    var sp = sentence.split(','); \n" +
                    "    print(sentence.split); \n" +
                    "    print(sp); \n" +
                    "    return sp; \n" +
                    "});\n" +

                    "print('rdd0:' + rdd0.count());" +
                    "print('rdd0 collected:' +rdd0.collect());" +
                    //to be removed
                    "\n" +
                    //"var turf = require('./lib/turf.min.js');" +

                    "var rdd2 = application.flatMap(function(sentence) {\n" +
                    "    var sp = sentence.split(','); \n" +
                    "    print(sentence.split); \n" +
                    "    print(sp); \n" +
                    "    return sp; \n" +
                    "});\n" +

                    "\n" +
                    "print('rdd2:' + rdd2.count());" +
                    "print('rdd2 collected:' +JSON.stringify(rdd2.collect()));" +
                    "\n" +
                    "var rdd3 = rdd2.filter(function(word) {\n" +
                    //"    print(word);  \n"+
                    "    print(JSON.stringify(word));  \n" +
                    "    return word.trim().length > 0;\n" +
                    "});\n" +
                    "\n" +
                    "\n" +
                    "var rdd4 = rdd3.mapToPair(function(word, Tuple2) {\n" +
                    "    return new Tuple2(word, 1);\n" +
                    "}, [Tuple2]);\n" +
                    "\n" +
                    "\n" +
                    "\n" +
                    "var rdd5 = rdd4.reduceByKey(function(a, b) {\n" +
                    "    return a + b;\n" +
                    "});\n" +
                    "\n" +
                    "\n" +
                    "var rdd6 = rdd5.mapToPair(function(tuple, Tuple2) {\n" +
                    "    return new Tuple2(tuple._2() + 0.0, tuple._1());\n" +
                    "}, [Tuple2]);\n" +
                    "\n" +
                    "\n" +
                    "var rdd7 = rdd6.sortByKey(false);\n" +
                    "\n" +
                    "\n" +
                    "var result = rdd7.take(1);\n" +
                    "\n" +
                    "return result[0];";

            //this overrides the code
            Metric metric = metricsDefinition.getMetricByName("simpleMetric");
            metric.setCode(code);
            Scope textLength = metricsDefinition.getScopeByName("lengthScope");
            textLength.setCode("application.count()>0");

            RenderConfig config = ((BaseEngine) metricsEngine).getConfig();
            //RenderConfig config = new RenderConfig();
            config.setRenderSparkContextCreation(true);

            config.setDataFile("D:\\\\Projects\\\\GeoFriends\\\\GeoFences\\\\metrics-engine\\\\src\\\\main\\\\resources\\\\samples\\\\sample1.txt");
            //config.setDataFile(".\\\\samples\\\\sample.txt");

            String script = metricsEngine.renderScript(metricsDefinition, config);

            log(script);

            Object metricResult = metricsEngine.eval(metricsDefinition, metric, config);

            log(metricResult);
        } finally {
            metricsEngine.stop();
        }

    }


    @Test
    @Ignore //must be ignored as turf cant be used
    public void sampleMetricsWithTurf() throws IOException, ProcessingException {
        MetricsEngine engine = new DefaultMetricsEngine();

        MetricsDefinition metricsDefinition = MetricUtils.withMetricDefinition("sample-documents/sample-metric.json");

        //language="JS"
        String turfCode = "var poly = {\n" +
                "  \"type\": \"Feature\",\n" +
                "  \"properties\": {},\n" +
                "  \"geometry\": {\n" +
                "    \"type\": \"Polygon\",\n" +
                "    \"coordinates\": [[\n" +
                "      [105.818939,21.004714],\n" +
                "      [105.818939,21.061754],\n" +
                "      [105.890007,21.061754],\n" +
                "      [105.890007,21.004714],\n" +
                "      [105.818939,21.004714]\n" +
                "    ]]\n" +
                "  }\n" +
                "};\n" +
                "\n" +
                "var centroidPt = turf.centroid(poly);\n" +
                "\n" +
                "var result = {\n" +
                "  \"type\": \"FeatureCollection\",\n" +
                "  \"features\": [poly, centroidPt]\n" +
                "};" +
                "" +
                "return JSON.stringify(result);";

        Metric metric = metricsDefinition.getMetricByName("simpleMetric");
        metric.setCode(turfCode);

        RenderConfig config = new RenderConfig();
        config.setRenderSparkContextCreation(true);

        config.setDataFile("D:\\\\Projects\\\\GeoFriends\\\\GeoFences\\\\metrics-engine\\\\src\\\\main\\\\resources\\\\samples\\\\sample.txt");

        String script = engine.renderScript(metricsDefinition, config);

        log(script);

        try {

            Object metricResult = engine.eval(metricsDefinition, metric, config);

            log(metricResult);
        } finally {
            //engine.sto
        }

    }


}
