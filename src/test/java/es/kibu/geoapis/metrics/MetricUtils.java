package es.kibu.geoapis.metrics;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import es.kibu.geoapis.metrics.engine.MetricEvaluationResult;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.serialization.MetricsDefinitionReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 13/09/2016.
 */
public class MetricUtils {

    static Logger logger = LoggerFactory.getLogger(MetricUtils.class);

    public static MetricsDefinition withMetricDefinition(String fileName) throws ProcessingException, IOException {
        MetricsDefinitionReader metricsDefinitionReader = new MetricsDefinitionReader();
        return metricsDefinitionReader.readFromResources(TestUtils.class.getClassLoader(), fileName);
    }

    public static void dumpResult(Object result) {
        if (result instanceof Map) {
            for (Object o : ((Map) result).entrySet()) {
                Map.Entry entry = (Map.Entry)o;
                if (entry.getValue() instanceof MetricEvaluationResult) {
                    Object res = ((MetricEvaluationResult) entry.getValue()).getResult();
                    logger.debug(String.format("%s -> %s", entry.getKey(), res.toString()));
                }

            }
        }
    }

    public static Object getResultForMetric(String metric, Object result) {
        if (result instanceof Map) {
            for (Object o : ((Map) result).entrySet()) {
                Map.Entry entry = (Map.Entry)o;
                if (entry.getValue() instanceof MetricEvaluationResult) {
                    Object res = ((MetricEvaluationResult) entry.getValue()).getResult();
                    if (entry.getKey().toString().equalsIgnoreCase(metric)) {
                        //logger.debug(String.format("%s -> %s", entry.getKey(), res.toString()));
                        return  res;
                    }
                }

            }
        }
        return null;
    }
}
