package es.kibu.geoapis.metrics;

import akka.actor.UntypedActor;
import scala.concurrent.Future;

import java.util.concurrent.Callable;

import static akka.dispatch.Futures.future;
import static akka.pattern.Patterns.pipe;

//#backend
public class FactorialBackend extends UntypedActor {
    @Override
    public void onReceive(Object message) {

        if (message instanceof Integer) {
            final Integer n = (Integer) message;
            Future<String> result = future(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    Object executionResult = App.getExecutionResult();
                    return executionResult.toString();
                    /*return "ok";*/
                }
            }, getContext().dispatcher());

            pipe(result, getContext().dispatcher()).to(getSender());

        } else {
            unhandled(message);
        }
    }

}
//#backend

