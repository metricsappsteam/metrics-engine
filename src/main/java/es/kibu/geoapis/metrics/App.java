package es.kibu.geoapis.metrics;

import es.kibu.geoapis.metrics.nashorn.MetricsSparkJs;
import es.kibu.geoapis.metrics.utils.Utils;
import org.eclairjs.nashorn.SparkJS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hello world!
 */
public class App {


    static Logger logger = LoggerFactory.getLogger(App.class);

    private static String getFileContent(Object app, String fileName) {
        ClassLoader classLoader = app.getClass().getClassLoader();
        return Utils.getFileContent(fileName, classLoader)/*getFileContent()*/;

    }


    private static void log(String message) {
        logger.debug(message);
    }

    public static void main(String[] args) {
        Object ret = getExecutionResult();
        log("Result:" + ret);
    }

    public static Object getExecutionResult() {
        App app = new App();
        String javascript = getFileContent(app, "scripts/word_count.js");
        return executeJavaScript(javascript);
    }

    public static Object executeJavaScript(String javascript) {
        SparkJS js = new SparkJS();

        Object ret;
        try {
            ret = js.eval(javascript);
        } catch (Exception e) {
            System.err.println(e);
            ret = e;
        }
        return ret;
    }


    public static Object evaluateWorldCount() {

        MetricsSparkJs js = new MetricsSparkJs();
        App app = new App();
        String javascript = getFileContent(app, "scripts/word_count1.js");
        js.eval(javascript);
        Object ret = js.invokeFunction("WordCount");
        log("Stopping context");
        js.stopContext();

        return ret;

    }


}
