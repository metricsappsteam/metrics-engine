package es.kibu.geoapis.metrics;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import es.kibu.geoapis.metrics.actors.ScriptBackend;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

/**
 * Created by lrodr_000 on 18/08/2016.
 */
public class ScriptBackendMain {


    public static class ReceiverActor extends UntypedActor {

        LoggingAdapter log = Logging.getLogger(getContext().system(), this);

        public ReceiverActor() {

        }

        @Override
        public void onReceive(Object message) throws Exception {
            log.debug("Received result: {}", message);
        }
    }

    public static void main(String[] args) {

        final ActorSystem system = ActorSystem.create("SimpleSystem");

        boolean fake = false;

        if (args.length > 0) {
            if (args[0].equalsIgnoreCase("fake")) {
                fake = true;
            }
        }

        final ActorRef scriptBackend = system.actorOf(Props.create(ScriptBackend.class, fake), "scriptBackend");
        final ActorRef receiverActor = system.actorOf(Props.create(ReceiverActor.class), "receiverBackend");

        final LoggingAdapter log = system.log();


        system.scheduler().scheduleOnce(Duration.create(50, TimeUnit.MILLISECONDS), /*Duration.create(1000, TimeUnit.MILLISECONDS),*/
                new Runnable() {
                    @Override
                    public void run() {
                        log.debug("scheduled exec command ...");
                        scriptBackend.tell("exec", receiverActor);
                    }
                }, system.dispatcher());


        try {
            system.awaitTermination(new FiniteDuration(20, TimeUnit.SECONDS));
        } catch (Exception e) {
            e.printStackTrace();
            system.shutdown();
        }

    }

}
