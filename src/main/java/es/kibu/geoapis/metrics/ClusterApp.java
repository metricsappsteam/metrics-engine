package es.kibu.geoapis.metrics;

import akka.actor.ActorSystem;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

public class ClusterApp {

    public static void main(String[] args) {
        // starting 3 backend nodes and 1 frontend node
        FactorialBackendMain.main(new String[]{"2551"});
        FactorialBackendMain.main(new String[]{"2552"});
        FactorialBackendMain.main(new String[0]);
        ActorSystem actorSystem = FactorialFrontendMain.runFrontEnd();

        actorSystem.awaitTermination(new FiniteDuration(2, TimeUnit.MINUTES));
    }
}
