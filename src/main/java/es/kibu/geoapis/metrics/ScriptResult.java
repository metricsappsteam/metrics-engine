package es.kibu.geoapis.metrics;

import java.io.Serializable;
import java.math.BigInteger;

public class ScriptResult implements Serializable {

    private static final long serialVersionUID = 1L;
    public final int n;
    public final String result;

    ScriptResult(int n, String result) {
        this.n = n;
        this.result  = result;
    }
}
