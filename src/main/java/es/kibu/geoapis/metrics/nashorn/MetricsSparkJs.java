package es.kibu.geoapis.metrics.nashorn;

import org.apache.spark.api.java.JavaSparkContext;
import org.eclairjs.nashorn.NashornEngineSingleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.*;
import java.io.Serializable;

/**
 * Created by lrodr_000 on 19/08/2016.
 */
public class MetricsSparkJs implements Serializable {


    Logger logger = LoggerFactory.getLogger(MetricsSparkJs.class);
    static final String LOG_PROPERTIES_FILE = "conf/Log4J.properties";
    public static final String SPARK_CONTEXT_VAR = "sc";
    public static final String METRIC_FN_VAR = "metric";

    JavaSparkContext testingContext = null;
    ScriptEngine engine;

    public MetricsSparkJs() {
        this(null);
    }

    public MetricsSparkJs(JavaSparkContext context) {
        this.testingContext = context;
        engine = NashornSingleton.getNewEngine();//NashornEngineSingleton.getEngine();
    }

    public boolean isTestingContextDefined() {
        return testingContext != null;
    }

/*    private static void initializeLogger() {
        Properties logProperties = new Properties();

        try {
            InputStreamReader engine = new InputStreamReader(org.eclairjs.nashorn.SparkJS.class.getResourceAsStream("conf/Log4J.properties"));
            logProperties.load(engine);
            PropertyConfigurator.configure(logProperties);
        } catch (IOException var2) {
            throw new RuntimeException("Unable to load logging property conf/Log4J.properties");
        }
    }*/

    /*private Object loadJS(String jsFile, String[] args) {
        Object ret;
        try {
            ScriptEngine engine = NashornEngineSingleton.getEngine();

            ScriptContext defCtx = engine.getContext();
            defCtx.getBindings(ScriptContext.ENGINE_SCOPE).put("args", args);
            ret = engine.eval("load(\'" + jsFile + "\');");
        } catch (ScriptException var6) {
            ret = var6;
            logger.debug(var6.toString());
        }
        return ret;
    }*/


    /*public Object eval(String javaScript) {
        return eval(null, javaScript);
    }
  */

    public Object eval(/*ScriptEngine engine, */String javaScript) {
        Object ret;
        try {
            if (isTestingContextDefined()) {
                prepareAndBindExternalContext(engine, getSparkContextVarName());
            } else {
                engine.eval(this.createSparkContextExpression());
            }

            ret = engine.eval(javaScript);

        } catch (ScriptException ex) {
            logger.debug(ex.getMessage());
            ret = ex;
        }
        return ret;
    }


    private String getMaster() {
        String master = System.getProperty("spark.master");
        if (master == null) {
            master = System.getenv("MASTER");
            if (master == null) {
                master = "local[*]";
            }
        }

        return master;
    }

    private String getAppName() {
        String name = System.getProperty("spark.appname");
        if (name == null) {
            name = System.getenv("APP_NAME");
            if (name == null) {
                name = "eclairJS REPL application";
            }
        }
        return name;
    }


    private String sparkContextVarName;

    public void setSparkContextVarName(String sparkContextVarName) {
        this.sparkContextVarName = sparkContextVarName;
    }

    public String getSparkContextVarName() {
        return (sparkContextVarName == null || sparkContextVarName.isEmpty()) ? SPARK_CONTEXT_VAR : sparkContextVarName;
    }

    private String createSparkContextExpression() {
        String sparkContext = prepareSparkContextExpression();
        sparkContext = sparkContext + "var SparkContext = require(\'eclairjs/SparkContext\'); var " + getSparkContextVarName() + " = new SparkContext(conf);";
        return sparkContext;
    }

    private String prepareSparkContextExpression() {
        String master = this.getMaster();
        String name = this.getAppName();
        String sparkContext = "var SparkConf = require(\'eclairjs/SparkConf\'); var conf = new SparkConf()";
        sparkContext = sparkContext + ".setMaster(\"" + master + "\")";
        if (name != null) {
            sparkContext = sparkContext + ".setAppName(\"" + name + "\")";
        }

        sparkContext = sparkContext + "; ";
        return sparkContext;
    }

    public Object invokeFunction(String functionName) {
        return invokeFunction(functionName, false);
    }

    public Object invokeFunction(String functionName, boolean createContext) {
        ScriptEngine engine = this.engine;
        Object ret = null;

        try {
            if (isTestingContextDefined()) {
                prepareAndBindExternalContext(engine, getSparkContextVarName());
            } else {
                //set the created spark context
                //in case the context is not defined
                if (createContext) {
                    this.eval(/*engine, */this.createSparkContextExpression());
                }
            }
            ret = ((Invocable) engine).invokeFunction(functionName);

        } catch (ScriptException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } finally {

        }
        return ret;
    }

    private void prepareAndBindExternalContext(ScriptEngine engine, String sparkContextVarName) throws ScriptException {
        if (!isTestingContextDefined()) {
            String sparkContextPreparation = prepareSparkContextExpression();
            engine.eval(sparkContextPreparation);
        } else
            engine.getContext().getBindings(ScriptContext.ENGINE_SCOPE).put("kernel", new ExternalContextKernel(testingContext));
    }

    public static class ExternalContextKernel implements Serializable {
        private JavaSparkContext sc = null;

        public ExternalContextKernel(String appName) {
            this.sc = new JavaSparkContext("local[*]", appName);
        }

        public ExternalContextKernel(JavaSparkContext context) {
            this.sc = context;
        }

        public JavaSparkContext javaSparkContext() {
            return this.sc;
        }
    }

    public Bindings getBindings() {
        return engine.getContext().getBindings(ScriptContext.ENGINE_SCOPE);
    }


    public void stopContext() {
        ScriptEngine engine = NashornEngineSingleton.getEngine();
        try {

            String statement = String.format("print(%s);", getSparkContextVarName());
            String stop = getSparkContextVarName() + ".stop();";
            String delete = String.format("%s = null;", getSparkContextVarName());
            String typeof = String.format("print(typeof %s);", getSparkContextVarName());
            String print = String.format("print(%s);", getSparkContextVarName());

            String fragment = String.format("%s \n %s \n %s \n %s \n %s", statement, stop, delete, typeof, print);
            logger.debug("Stoping context...");
            engine.eval(fragment);
            logger.debug("Context stopped");

        } catch (ScriptException e) {
            logger.debug("Error occurred while stopping context", e);

        }

    }


}
