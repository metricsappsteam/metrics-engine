package es.kibu.geoapis.metrics.nashorn;

import org.eclairjs.nashorn.SparkBootstrap;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by lrodr_000 on 15/09/2016.
 */
public class NashornSingleton {

    static ScriptEngine engine = null;
    static Boolean sparkJSLoaded = Boolean.valueOf(false);

    public NashornSingleton() {
    }

    public static ScriptEngine getNewEngine() {
        reset();
        getEngine();
        return engine;
    }



    public static ScriptEngine getEngine() {
        if (engine == null) {
            ScriptEngineManager engineManager = new ScriptEngineManager();
            engine = engineManager.getEngineByName("nashorn");
        }

        loadSparkJS();
        return engine;
    }

    public static void loadSparkJS() {
        if (!sparkJSLoaded.booleanValue()) {
            (new SparkBootstrap()).load(engine);
            sparkJSLoaded = Boolean.valueOf(true);
        }

    }

    public static void setEngine(ScriptEngine e) {
        engine = e;
        sparkJSLoaded = Boolean.valueOf(true);
    }

    public static void reset() {
        engine = null;
        sparkJSLoaded = false;
    }

   /* public static void main(String[] args) {
        try {
            getEngine().eval(new FileReader(args[0]));
        } catch (FileNotFoundException var2) {
            var2.printStackTrace();
        } catch (ScriptException var3) {
            var3.printStackTrace();
        }

    }*/
}
