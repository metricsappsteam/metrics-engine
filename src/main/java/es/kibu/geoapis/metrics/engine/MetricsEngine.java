package es.kibu.geoapis.metrics.engine;

import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

/**
 * Created by lrodr_000 on 21/08/2016.
 */
public interface MetricsEngine {

    void registerInMetricsContext(String propertyName, String propertyValue);
    Object eval(MetricsDefinition metricsDefinition);
    Object eval(MetricsDefinition metricsDefinition, Metric metric, RenderConfig config);
    String renderScript(MetricsDefinition metricsDefinition, RenderConfig config);
    String renderScript(MetricsDefinition metricsDefinition);
    void setTesting(boolean testing);
    void stop();

}
