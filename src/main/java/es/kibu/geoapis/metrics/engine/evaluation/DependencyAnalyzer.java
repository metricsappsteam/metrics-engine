package es.kibu.geoapis.metrics.engine.evaluation;

import org.jgrapht.DirectedGraph;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by lrodr_000 on 12/09/2016.
 */
public interface DependencyAnalyzer {
    UsageStats uses(String varUsed, String varUser);

    /**
     * The usage graph show the usages between different variables.
     * If variable 'a' is used by variable 'b' then there is an edge
     * from 'a' to 'b'
     */

    DirectedGraph<String, UsageStats> buildUsageGraphFor(List<String> variablesOrFunctions);

    class CycleInfo {

        public List<DependencyExtractionVisitor.Scope> getScopeList() {
            return scopeList;
        }

        public void setScopeList(List<DependencyExtractionVisitor.Scope> scopeList) {
            this.scopeList = scopeList;
        }

        List<DependencyExtractionVisitor.Scope> scopeList;

        public CycleInfo(String varName, List<DependencyExtractionVisitor.Scope> scopeList) {
            this.varName = varName;
            this.scopeList = scopeList;
        }

        public String getVarName() {
            return varName;
        }

        public void setVarName(String varName) {
            this.varName = varName;
        }

        String varName;

    }

    public static class UsageStats {
        String usedVar;

        Stack<Object> trace = new Stack();

        public UsageStats(String usedVar) {
            this.usedVar = usedVar;
        }

        void incDep(Object step) {
            trace.push(step);
        }

        void decDep(Object step) {
            trace.pop();
        }

        public boolean isUsed() {
            return depth() > 0;
        }

        public int depth() {
            return trace.size();
        }

        @Override
        public String toString() {
            String str = "";
            String sep = "";
            for (Object o : trace) {
                if (o instanceof DependencyExtractionVisitor.Scope) {
                    str += sep + ((DependencyExtractionVisitor.Scope) o).name.toString();
                    sep = " => ";
                }
            }
            return String.format("[%s] -> %s", str, usedVar);
        }

        public boolean areThereCycles() {
            return cicles.size()>0;
        }

        public void setCicleInfo(String onVariable, List<DependencyExtractionVisitor.Scope> scopeList){
             cicles.add(new CycleInfo(onVariable, scopeList));
        }

        public List<CycleInfo> getCicles() {
            return cicles;
        }

        private List<CycleInfo> cicles = new ArrayList<>();

    }
}
