package es.kibu.geoapis.metrics.engine;

/**
 * Created by lrodr_000 on 22/08/2016.
 */
public class MetricsEngineFactory {

    private static MetricsEngine _instance;

    public static MetricsEngine getMetricEngine() {
        if (_instance == null){
            _instance = new DefaultMetricsEngine();
        }
        return _instance;
    }
}
