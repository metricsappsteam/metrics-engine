package es.kibu.geoapis.metrics.engine;

import es.kibu.geoapis.metrics.nashorn.MetricsSparkJs;

/**
 * This is the configuration to pass to the render
 */
public class RenderConfig {

    public String sparkVarName = MetricsSparkJs.SPARK_CONTEXT_VAR;

    public boolean renderSparkContextCreation = false;
    public String sparkApplicationName = "JavaScript Metric Evaluation Script";

    public String dataFile = "datafile.txt";

    public String getDataFile() {
        return dataFile;
    }

    public void setDataFile(String dataFile) {
        this.dataFile = dataFile;
    }

    public boolean testing = false;

    public boolean isTesting() {
        return testing;
    }

    public void setTesting(boolean testing) {
        this.testing = testing;
    }

    public String getSparkApplicationName() {
        return sparkApplicationName;
    }

    public void setSparkApplicationName(String sparkApplicationName) {
        this.sparkApplicationName = sparkApplicationName;
    }

    public boolean isRenderSparkContextCreation() {
        return renderSparkContextCreation;
    }

    public void setRenderSparkContextCreation(boolean renderSparkContextCreation) {
        this.renderSparkContextCreation = renderSparkContextCreation;
    }


    public String getSparkVarName() {
        return sparkVarName;
    }

    public String getMetricContextVarName() {
        return metricContextVarName;
    }

    private String  metricContextVarName = "mc";

    private boolean debugging;

    public boolean isDebugging() {
        return debugging;
    }

    public void setDebugging(boolean debugging) {
        this.debugging = debugging;
    }
}
