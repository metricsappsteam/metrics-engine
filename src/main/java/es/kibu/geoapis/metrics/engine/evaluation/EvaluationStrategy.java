package es.kibu.geoapis.metrics.engine.evaluation;

import es.kibu.geoapis.objectmodel.Metric;

/**
 * For now this strategy is in charge of evaluating the metrics in the order defined in the strategy
 */

public interface EvaluationStrategy {
     Metric getNextMetric();
     void reset();
}
