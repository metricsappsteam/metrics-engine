package es.kibu.geoapis.metrics.engine;

import es.kibu.geoapis.objectmodel.Metric;

import java.io.Serializable;

/**
 * Created by lrodr_000 on 07/09/2016.
 */
public class MetricEvaluationResult implements Serializable {

    Object result;

    Metric metric;

    public Metric getMetric() {
        return metric;
    }

    public Object getResult() {
        return result;
    }


    public MetricEvaluationResult(Metric metric, Object result) {
        this.metric = metric;
        this.result = result;
    }
}
