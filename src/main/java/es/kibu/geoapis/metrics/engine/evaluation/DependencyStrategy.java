package es.kibu.geoapis.metrics.engine.evaluation;

import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;
import es.kibu.geoapis.objectmodel.Variable;
import org.jgrapht.DirectedGraph;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * Created by lrodriguez2002cu on 08/09/2016.
 */
public class DependencyStrategy implements EvaluationStrategy {

    MetricsDefinition definition;

    DependencyAnalyzer analyzer;

    DirectedGraph<String, DefaultJSDependencyAnalyzer.UsageStats> usageGraph;

    Variable basis;

    private List<String> executionStrategy;


    public Variable getBasis() {
        return basis;
    }

    public void setBasis(Variable basis, StrategyType strategyType) {
        this.basis = basis;
        executionStrategy = calculateStrategy(basis.getName(), strategyType);
    }

    public void setBasis(StrategyType strategyType) {
        this.basis = null;
        List<String> variablesNames = new ArrayList<>();
        getAllVarNames(definition, variablesNames);
        executionStrategy = calculateStrategy(variablesNames, strategyType);
    }


    public DependencyStrategy(MetricsDefinition definition, DependencyAnalyzer analyzer) {
        this.definition = definition;
        this.analyzer = analyzer;
    }

    public void init() {
        List<String> allNames = getAllNames(definition);
        usageGraph = analyzer.buildUsageGraphFor(allNames);
    }

    public enum StrategyType {ONLY_DIRECT_CALLERS, ALL_WHO_USES};

    public List<String> calculateStrategy(String name, StrategyType strategyType) {
        List<String> names = new ArrayList<>();
        names.add(name);
        return calculateStrategy(names, strategyType);
    }

    public List<String> calculateStrategy(List<String> names, StrategyType strategyType) {
        //String name = getBasis().getName();

        List<String> usersVarList = getDependencies(names, strategyType);

        /**
         * This provides some order that indicates the usage between different functions, as
         * a general rule if a depends on b, then b will  be solved before
         */
        usersVarList.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (usageGraph.containsEdge(o1, o2)) {
                    return -1;
                } else {
                    if (usageGraph.containsEdge(o2, o1)) {
                        return 1;
                    }
                }
                return 0;
            }
        });

        return usersVarList;
    }

    private List<String> getDependencies(List<String> names, StrategyType strategyType) {

        List<String> usersVarList = new ArrayList<>();

        for (String name : names) {
            Set<DefaultJSDependencyAnalyzer.UsageStats> usageStatses = usageGraph.outgoingEdgesOf(name);
            for (DefaultJSDependencyAnalyzer.UsageStats usageStats : usageStatses) {
                //is not direct callers or is direct caller and (depth == 1) usage
                boolean condition = (strategyType != StrategyType.ONLY_DIRECT_CALLERS) || usageStats.depth() == 1;
                if (condition) {
                    String userVar = usageGraph.getEdgeTarget(usageStats);
                    //if the variable is not already included
                    if (!usersVarList.contains(userVar)) usersVarList.add(userVar);
                }
            }
        }
        return usersVarList;
    }

    List<String> getAllNames(MetricsDefinition definition) {
        List<String> variablesNames = new ArrayList<>();

        getAllVarNames(definition, variablesNames);

        for (Metric metric : definition.getMetrics()) {
            variablesNames.add(metric.getName());
        }

        return variablesNames;
    }

    private void getAllVarNames(MetricsDefinition definition, List<String> variablesNames) {
        for (Variable variable : definition.getVariables()) {
            variablesNames.add(variable.getName());
        }
    }

    int current = 0;

    @Override
    public Metric getNextMetric() {

        if (current < executionStrategy.size()) {
            String fnName = executionStrategy.get(current);
            Metric metricByName = definition.getMetricByName(fnName);
            current++;
            return metricByName;
        } else return null;
    }

    @Override
    public void reset() {
        current = 0;
    }
}
