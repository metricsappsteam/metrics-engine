package es.kibu.geoapis.metrics.engine;

import es.kibu.geoapis.metrics.engine.evaluation.*;
import es.kibu.geoapis.metrics.nashorn.MetricsSparkJs;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 21/08/2016.
 */
public class DefaultStatefulMetricsEngine extends BaseEngine implements StatefulMetricsEngine {

    MetricsDefinition metricsDefinition;

    DependencyAnalyzer dependencyAnalyzer;

    String script;


    public DefaultStatefulMetricsEngine() {
          this.config = new RenderConfig();
    }

    public DefaultStatefulMetricsEngine(boolean testing) {
        this();
        config.setTesting(testing);
    }

    @Override
    public void init(MetricsDefinition metricsDefinition, boolean preevaluate) {
        init(metricsDefinition, config, preevaluate);

    }

    private void init(MetricsDefinition metricsDefinition, RenderConfig config, boolean preEvaluate) {
        this.metricsDefinition = metricsDefinition;
        this.config = config;

        String fullScript = getScript();

        //evaluate the script so that it is already cached in the engine
        bindMetricsContext();

        if (preEvaluate) {
            getMetricsSparkJs().eval(fullScript);
        }


        initDependencyAnalyzer();

    }

    @Override
    public MetricsDefinition getMetricsDefinition() {
        return getMetricsDefinition();
    }

    private void initDependencyAnalyzer(){
        dependencyAnalyzer = new DefaultJSDependencyAnalyzer(this.getScript());
    }

    @Override
    public void init(MetricsDefinition metricsDefinition) {
        init(metricsDefinition, config, true);
    }


    @Override
    public String getScript() {

        if (metricsDefinition == null) throw  new RuntimeException("Metric definition must be defined through 'init()'");

        if (script == null) {
            RenderConfig config = (this.config == null) ? new RenderConfig() : this.config;
            script = renderScript(this.metricsDefinition, config);
        }
        return script;
    }

    @Override
    public Object eval(Metric metric) {
        Map<String, Object> results = new HashMap();
        Object ret = getMetricsSparkJs().invokeFunction(metric.getName());
        results.put(metric.getName(), new MetricEvaluationResult(metric, ret));
        return results;
    }

    @Override
    public Object evalAllChanged(String varName) {
        DependencyStrategy strategy = new DependencyStrategy(metricsDefinition, dependencyAnalyzer);
        strategy.setBasis(metricsDefinition.getVariableByName(varName), DependencyStrategy.StrategyType.ALL_WHO_USES);
        Map<String, Object> results = eval(strategy);
        return results;
    }

    @Override
    public Object evalAllInmediate(String varName) {
        DependencyStrategy strategy = new DependencyStrategy(metricsDefinition, dependencyAnalyzer);
        strategy.setBasis(metricsDefinition.getVariableByName(varName), DependencyStrategy.StrategyType.ONLY_DIRECT_CALLERS);
        Map<String, Object> results = eval(strategy);
        return results;
    }

    @Override
    public Object eval() {
        if (metricsDefinition!= null) throw new RuntimeException("Metric definition is null. init() must be called first");
        //TODO: setup the variable name here.
        String varName = "";
        DependencyStrategy strategy = new DependencyStrategy(metricsDefinition, dependencyAnalyzer);
        strategy.setBasis(metricsDefinition.getVariableByName(varName), DependencyStrategy.StrategyType.ONLY_DIRECT_CALLERS);
        Map<String, Object> results = eval(strategy);
        return results;
    }

    private Map<String, Object> eval(DependencyStrategy strategy) {
        Map<String, Object> results = new HashMap();

        Metric metric = null;
        do {
            metric = strategy.getNextMetric();
            if (metric != null) {
                Object ret = getMetricsSparkJs().invokeFunction(metric.getName());
                results.put(metric.getName(), new MetricEvaluationResult(metric, ret));
            }
        }
        while (metric != null);
        return results;
    }

    @Override
    public void stop() {
        getMetricsSparkJs().stopContext();
    }

}
