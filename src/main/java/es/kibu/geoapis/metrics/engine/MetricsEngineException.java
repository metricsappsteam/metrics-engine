package es.kibu.geoapis.metrics.engine;

/**
 * Created by lrodr_000 on 21/08/2016.
 */
public class MetricsEngineException extends RuntimeException {
    public MetricsEngineException(Exception e) {
        super(e);
    }

    public MetricsEngineException(String s) {
        super(s);
    }
}
