package es.kibu.geoapis.metrics.engine;

import org.eclairjs.nashorn.wrap.WrappedClass;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodr_000 on 17/09/2016.
 */
public class MetricsContext extends WrappedClass implements Serializable {

     Map<String, Object> properties = new HashMap();

     void addProperty(String property, Object value) {
        properties.put(property, value);
     }

    @Override
    public Object getJavaObject() {
        return this;
    }

    @Override
    public String getClassName() {
        return this.getClassName();
    }

    @Override
    public boolean checkInstance(Object o) {
        return this.equals(o);
    }

    @Override
    public Object getMember(String name) {
        if (properties.containsKey(name)){
            return properties.get(name);
        }
        return super.getMember(name);
    }

    @Override
    public String toString() {
        return properties.toString();
    }
}
