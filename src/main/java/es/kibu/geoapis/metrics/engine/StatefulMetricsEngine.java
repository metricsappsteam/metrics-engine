package es.kibu.geoapis.metrics.engine;

import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

/**
 * Created by lrodr_000 on 21/08/2016.
 */
public interface StatefulMetricsEngine {

    void init(MetricsDefinition metricsDefinition, boolean preEvaluate);
    void init(MetricsDefinition metricsDefinition);

    void registerInMetricsContext(String propertyName, String propertyValue);

    String getScript();

    Object eval(Metric metric);

    Object evalAllChanged(String varName);

    Object evalAllInmediate(String varName);

    Object eval();

    void stop();
}
