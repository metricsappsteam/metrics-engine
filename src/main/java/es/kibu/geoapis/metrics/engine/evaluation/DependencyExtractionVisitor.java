package es.kibu.geoapis.metrics.engine.evaluation;

import jdk.nashorn.internal.ir.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by lrodriguez2002cu on 13/09/2016.
 */
public class DependencyExtractionVisitor extends MoreComplexNodeVisitor {

    static Logger logger = LoggerFactory.getLogger(DependencyExtractionVisitor.class);

    Stack<Scope> scopeStack = new Stack<>();

    List<Scope> functionsList = new ArrayList<>();

    public static class OverflowDetector {

        List<String> callStack = new ArrayList<>();

        public void reset() {
            callStack.clear();
        }

        public void add(Scope scope) {
            callStack.add(scope.name);
        }

        public boolean exists(String functionName) {
            return callStack.contains(functionName);
        }

        public void add(List<Scope> calledScopes) {
            if (calledScopes == null) return;
            for (Scope calledScope : calledScopes) {
                add(calledScope);
            }
        }
    }

    class VariableUsage {

        String name;
        boolean wasDeclared;
        boolean isFunctionCall;
        int usageCount;

        public VariableUsage(String name, boolean wasDeclared, boolean isFunctionCall) {
            this.name = name;
            this.wasDeclared = wasDeclared;
            this.isFunctionCall = isFunctionCall;
            usageCount = 1;
        }

        @Override
        public String toString() {
            return "VariableUsage{" +
                    "name='" + name + '\'' +
                    ", wasDeclared=" + wasDeclared +
                    ", usageCount=" + usageCount +
                    '}';
        }
    }

    class Link {

        String assigned;
        List<VariableUsage> usedVars = new ArrayList<>();

        public Link(String assigned) {
            this.assigned = assigned;
        }

        void addUsed(String used) {
            if (!used.equalsIgnoreCase(assigned)) {
                VariableUsage variableUsage = new VariableUsage(used, false, false);
                usedVars.add(variableUsage);
            }
        }

        boolean hasLink(String assignedVar, String usedVar) {
            if (assignedVar.equalsIgnoreCase(assignedVar)) {
                for (VariableUsage var : usedVars) {
                    if (var.name.equalsIgnoreCase(usedVar)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    public static class UsageDescriptor {

        public List getSequence() {
            return sequence;
        }

        List sequence = new ArrayList();

        public int getDepth() {
            return depth;
        }

        int depth = 0;

        public UsageDescriptor() {
        }

        public UsageDescriptor(boolean used) {
            this.used = used;
        }

        public void setUsed(boolean used) {
            this.used = used;
        }

        public boolean isUsed() {
            return used;
        }

        boolean used;

        void incDepth(Object elem) {
            sequence.add(elem);
            depth++;
        }

        boolean usedDirectly() {
            return depth == 0 && used;
        }
    }

    class Scope {

        String name;
        Scope parent;
        List<Scope> childScopes = new ArrayList<>();
        List<VariableUsage> usages = new ArrayList<>();

        List<Link> linkages = new ArrayList<>();
        Stack<Link> linkStack = new Stack<>();

        public void openLink(String var) {
            linkStack.push(new Link(var));
        }

        public void closeLink(String varAssignedName) {
            if (!linkStack.empty()) {
                Link link = linkStack.pop();
                assert varAssignedName.equalsIgnoreCase(link.assigned);
                linkages.add(link);
            }
        }

        public void addToCurrentLink(String varName) {
            if (!linkStack.empty()) {
                linkStack.peek().addUsed(varName);
            }
        }


        public List<Scope> getChildScopes() {
            return childScopes;
        }

        private List<Scope> calledScopes;

        public List<Scope> calls() {
            if (calledScopes == null) {
                calledScopes = new ArrayList<>();

                List<VariableUsage> calls = new ArrayList<>();
                for (VariableUsage usage : usages) {
                    if (usage.isFunctionCall) {
                        calls.add(usage);
                    }
                }

                Scope parent = this;
                //search for the definitions of the called functions in the current scope and the parent scopes.
                while (parent != null) {
                    for (VariableUsage call : calls) {
                        for (Scope childScope : parent.childScopes) {
                            if (childScope.name.equals(call.name)) {
                                calledScopes.add(childScope);
                            }
                        }
                    }
                    parent = parent.getParent();
                }

            }

            return calledScopes;
        }


        public void addChildFunction(Scope fn) {
            childScopes.add(fn);
        }

        public boolean isRoot() {
            return parent == null;
        }

        public Scope getParent() {
            return parent;
        }

        public void setParent(Scope parent) {
            this.parent = parent;
            if (parent != null) {
                this.parent.addChildFunction(this);
            }
        }

        public Scope(String name) {
            this.name = name;
        }

        public void addVariableUsage(String name, boolean isDeclaration, boolean isFunction) {
            VariableUsage varibleUsage = getVaribleUsage(name);
            if (varibleUsage != null) {
                varibleUsage = getVaribleUsage(name);
                if (isDeclaration) {
                    varibleUsage.wasDeclared = isDeclaration;
                }
                if (isFunction) {
                    varibleUsage.isFunctionCall = isFunction;
                }
                varibleUsage.usageCount++;
            } else {
                usages.add(new VariableUsage(name, isDeclaration, isFunction));
            }
        }

        boolean isThereLinkage(String used, String external) {
            for (Link linkage : linkages) {
                if (linkage.hasLink(used, external)) {
                    return true;
                }
            }
            return false;
        }

        public UsageDescriptor findLinkage(String used, String external) {
            Scope scope = this;
            UsageDescriptor stats = new UsageDescriptor();
            do {

                if (scope.isThereLinkage(used, external)) {
                    stats.setUsed(true);
                    stats.incDepth(used);
                    return stats;
                }

                scope = scope.getParent();
            }
            while (scope != null);

            stats.setUsed(false);
            return stats;
        }

        private VariableUsage getVaribleUsage(String variableName) {
            for (VariableUsage usage : usages) {
                if (usage.name.equals(variableName)) return usage;
            }
            return null;
        }

        public UsageDescriptor usesExternal(String varName) {
            VariableUsage varibleUsage = getVaribleUsage(varName);

            if ((varibleUsage != null && !varibleUsage.wasDeclared)) {
                return new UsageDescriptor(true);
            }

            for (VariableUsage usage : usages) {
                UsageDescriptor useDesc = findLinkage(usage.name, varName);
                if (useDesc.isUsed()) {

                    return useDesc;
                }
            }
            return null;
        }

        public List<Scope> getScopeList(){
            List<Scope> result = new ArrayList<>();
            Scope scope = this;
            do {
                result.add(scope);
                scope = scope.getParent();
            }
            while (scope != null);
            return result;
        }

        @Override
        public String toString() {
            return "Scope{" +
                    "name='" + name + '\'' +
                    ", parent=" + parent +
                    ", usages=" + usages +
                    '}';
        }
    }

    public Scope findFunction(String fucntionName) {
        for (Scope scope : functionsList) {
            if (fucntionName.equals(scope.name)) return scope;
        }
        return null;
    }

    public List<Scope> getFunctionsList() {
        return functionsList;
    }

    @Override
    public boolean enterIdentNode(IdentNode identNode) {
        //super.enterIdentNode(identNode);
        //logger.debug(String.format("ident: '%s', declared: %b, internal: %b, asg: %b", identNode.getName(), identNode.isDeclaredHere(), identNode.isInternal(), identNode.isAssignment()));
        getCurrentScope().addVariableUsage(identNode.getName(), identNode.isDeclaredHere(), false);
        getCurrentScope().addToCurrentLink(identNode.getName());
        return true;
    }

    @Override
    protected boolean enterDefault(Node node) {
        logger.debug(String.format("default: %s (%s), asg: %b", node.toString(), node.getClass().getSimpleName(), node.isAssignment()));
        return super.enterDefault(node);
    }

    @Override
    public boolean enterFunctionNode(FunctionNode functionNode) {
        logger.debug("enter-func: " + functionNode.getName());
        Scope newScope = new Scope(functionNode.getName());
        newScope.setParent(scopeStack.size() == 0 ? null : getCurrentScope());
        scopeStack.push(newScope);
        return true;//super.enterFunctionNode(functionNode);
    }

    @Override
    public Node leaveFunctionNode(FunctionNode functionNode) {
        logger.debug("leave-func: " + functionNode.getName());
        Scope leftScope = scopeStack.pop();
        functionsList.add(leftScope);
        return super.leaveFunctionNode(functionNode);
    }

    @Override
    public boolean enterCallNode(CallNode callNode) {
        Scope currentScope = scopeStack.size() == 0 ? null : getCurrentScope();
        if (callNode.getFunction() instanceof IdentNode) {
            String name = getVarName(callNode);/*((IdentNode) callNode.getFunction()).getName();*/
            currentScope.addVariableUsage(name, false, true);
            //currentFunction.getVariableUsage()
        }
        return true;//super.enterCallNode(callNode);
    }

    @Override
    public boolean enterAccessNode(AccessNode accessNode) {
        return super.enterAccessNode(accessNode);
    }

    private Scope getCurrentScope() {
        return scopeStack.peek();
    }

    private String getVarName(Node node){

        if (node instanceof IdentNode){
            String name = ((IdentNode) node).getName();
            return name;
        }
        else {
            if (node instanceof AccessNode){
                Expression base = ((AccessNode) node).getBase();
                String baseName = "";
                if (base instanceof IdentNode) {baseName = ((IdentNode) base).getName() + ".";}
                String name =  baseName + ((AccessNode) node).getProperty();
                return name;
            }
            else  if (node instanceof CallNode ){
                return ((IdentNode) ((CallNode) node).getFunction()).getName();
            }
        }
        throw new RuntimeException("Unknow node type as to extracting the name");
    }

    @Override
    public boolean enterASSIGN(BinaryNode binaryNode) {
        Expression assignmentDest = binaryNode.getAssignmentDest();
        logger.debug(String.format("Enter Assigning: >> %s (%s)", assignmentDest, assignmentDest.getClass().getSimpleName()));
        if (assignmentDest instanceof IdentNode) {
            String name = getVarName(assignmentDest);/*((IdentNode) assignmentDest).getName();*/
            getCurrentScope().openLink(name);
        }
        else {
            if (assignmentDest instanceof AccessNode) {
                /*Expression base = ((AccessNode) assignmentDest).getBase();
                String baseName = "";
                if (base instanceof IdentNode) {baseName = ((IdentNode) base).getName() + ".";}
                String name =  baseName + ((AccessNode) assignmentDest).getProperty();*/
                String name = getVarName(assignmentDest);
                getCurrentScope().openLink(name);
            }
        }
        return true;//super.enterASSIGN(binaryNode);
    }

    @Override
    public Node leaveASSIGN(BinaryNode binaryNode) {
        Expression assignmentDest = binaryNode.getAssignmentDest();
        logger.debug("Exit Assigning: >>");
        if (assignmentDest instanceof IdentNode) {
            String name = getVarName(assignmentDest);/*((IdentNode) assignmentDest).getName();*/
            getCurrentScope().closeLink(name);
        }
        else {
            if (assignmentDest instanceof AccessNode) {
                /*Expression base = ((AccessNode) assignmentDest).getBase();
                String baseName = "";
                if (base instanceof IdentNode) {baseName = ((IdentNode) base).getName() + ".";}
                String name =  baseName + ((AccessNode) assignmentDest).getProperty();*/
                String name = getVarName(assignmentDest);
                getCurrentScope().closeLink(name);
            }
        }
        return super.leaveASSIGN(binaryNode);
    }

    @Override
    public boolean enterVarNode(VarNode varNode) {
        logger.debug(String.format("Enter Var: >> %s, ass: %b", varNode.getName(), varNode.isAssignment()));
        if (varNode.isAssignment()) {
            getCurrentScope().openLink(varNode.getAssignmentDest().getName());
        }
        return true;//super.enterVarNode(varNode);
    }

    @Override
    public Node leaveVarNode(VarNode varNode) {
        logger.debug(String.format("Exit Var: >> %s", varNode.getName()));
        if (varNode.isAssignment()) {
            getCurrentScope().closeLink(varNode.getAssignmentDest().getName());
        }
        return super.leaveVarNode(varNode);
    }

    public void visit(Node root) {
        root.accept(this);
    }

    @Override
    public String toString() {
        return "CustomVisitor{" +
                "scopeStack=" + scopeStack +
                ", functionsList=" + functionsList +
                '}';
    }

}
