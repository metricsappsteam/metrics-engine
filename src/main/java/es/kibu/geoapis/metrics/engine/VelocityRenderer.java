package es.kibu.geoapis.metrics.engine;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

/**
 * Created by lrodr_000 on 21/08/2016.
 */
public class VelocityRenderer {


    public String render(Map<String, Object> bindings) {

        Properties properties = new Properties();
        properties.setProperty("resource.loader", "file");
        properties.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(properties);

        VelocityContext context = new VelocityContext();

        for (String bindingName : bindings.keySet()) {
            context.put(bindingName, bindings.get(bindingName));
        }

        Template template = null;

        try {
            template = Velocity.getTemplate("templates/base.vm");
            if (template!=  null) {
                StringWriter sw = new StringWriter();
                template.merge(context, sw);
                return sw.toString();
            }
        } catch (Exception e) {
              throw new MetricsEngineException(e);
        }

        throw new MetricsEngineException("Unexpected situation");

    }

}
