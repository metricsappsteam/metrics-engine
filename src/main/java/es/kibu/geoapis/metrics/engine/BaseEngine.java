package es.kibu.geoapis.metrics.engine;

import com.google.gson.*;
import es.kibu.geoapis.data.DataUtils;
import es.kibu.geoapis.metrics.nashorn.MetricsSparkJs;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lrodriguez2002cu on 12/09/2016.
 */
public abstract class BaseEngine {

    RenderConfig config;

    private MetricsSparkJs metricsSparkJs;

    MetricsContext metricsContext = new MetricsContext();

    public static final String METRIC = "metric"; /*The metrics definition*/
    public static final String RENDER_CONFIG = "config";
    public static final String SAMPLE_DATA = "sampleData"; /*sample data rendered only in test mode*/


    private SampleData sampleData;

    /*public Object getSampleDataObject() {
        return sampleDataObject;
    }
    */
    public RenderConfig getConfig() {
        return config;
    }

    public void setConfig(RenderConfig config) {
        this.config = config;
    }


    public abstract MetricsDefinition getMetricsDefinition();

    public SampleData getSampleData(){
         if (sampleData == null) {
             sampleData = createSampleData(this.getMetricsDefinition());
         }
        return sampleData;
    }

    public void setSampleData (SampleData data){
        this.sampleData = data;
    }

    static class SampleData {
        String sampleDataStr;
        Object sampleDataObject;

        public SampleData(String sampleDataStr, Object sampleDataObject) {
            this.sampleDataStr = sampleDataStr;
            this.sampleDataObject = sampleDataObject;
        }
    }

    public static SampleData createSampleData(MetricsDefinition metricsDefinition) {
        String sampleData;
        Object sampleDataObject;
        Map<String, List<JsonElement>> stringListMap = DataUtils.generateListOfElements(metricsDefinition, 5);
            Gson gson = new Gson();
            JsonObject obj = new JsonObject();

            for (Map.Entry<String, List<JsonElement>> entry : stringListMap.entrySet()) {
                List<JsonElement> value = entry.getValue();
                JsonArray array = new JsonArray();
                for (JsonElement element : value) {
                    String json = gson.toJson(element);
                    array.add(new JsonPrimitive(json));
                }
                obj.add(entry.getKey(), array);
            }

            sampleDataObject = stringListMap;
            sampleData = gson.toJson(obj);

        return new SampleData(sampleData, sampleDataObject);

    }

    public String renderScript(MetricsDefinition metricsDefinition, RenderConfig config) {
        VelocityRenderer renderer = new VelocityRenderer();
        Map<String, Object> bindings = new HashMap();
        bindings.put(METRIC, metricsDefinition);
        bindings.put(RENDER_CONFIG, config);

        //if test mode, lets render some data.
        if (config.isTesting()) {
            bindings.put(SAMPLE_DATA, this.getSampleData().sampleDataStr);
        }

        return renderer.render(bindings);
    }

    public void setTesting(boolean testing) {
        config.setTesting(testing);
    }

    public MetricsSparkJs getMetricsSparkJs() {
        if (metricsSparkJs == null) {
            metricsSparkJs = new MetricsSparkJs();
        }
        return metricsSparkJs;
    }

    void bindMetricsContext() {
        getMetricsSparkJs().getBindings().put(config.getMetricContextVarName(), metricsContext);
    }

    public void registerInMetricsContext(String propertyName, String propertyValue) {
        metricsContext.addProperty(propertyName, propertyValue);
    }


}
