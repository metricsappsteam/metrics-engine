package es.kibu.geoapis.metrics.engine.evaluation;

import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

/**
 * Created by lrodr_000 on 07/09/2016.
 */
public class DefaultStrategy implements EvaluationStrategy {

    MetricsDefinition metricsDefinition;
    int current = 0;

    public DefaultStrategy(MetricsDefinition metricsDefinition) {
        this.metricsDefinition = metricsDefinition;
    }

    @Override
    public Metric getNextMetric() {
        if (current < metricsDefinition.getMetrics().size()) {
            Metric metric = metricsDefinition.getMetrics().get(current);
            return metric;
        } else {
            return null;
        }
    }

    @Override
    public void reset() {
        current = 0;
    }
}
