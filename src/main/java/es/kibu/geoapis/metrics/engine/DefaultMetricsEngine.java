package es.kibu.geoapis.metrics.engine;

import es.kibu.geoapis.metrics.engine.evaluation.DefaultStrategy;
import es.kibu.geoapis.metrics.engine.evaluation.EvaluationStrategy;
import es.kibu.geoapis.metrics.nashorn.MetricsSparkJs;
import es.kibu.geoapis.objectmodel.Metric;
import es.kibu.geoapis.objectmodel.MetricsDefinition;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lrodr_000 on 21/08/2016.
 */
public class DefaultMetricsEngine extends BaseEngine implements MetricsEngine {

    public DefaultMetricsEngine() {
        config = new RenderConfig();
    }

    public DefaultMetricsEngine(boolean testing) {
        this();
        config.setTesting(testing);
    }

    @Override
    public Object eval(MetricsDefinition metricsDefinition) {

        Map<String, Object> results = new HashMap();

        Object ret;
        MetricsSparkJs js = getMetricsSparkJs();
        String resultScript = renderScript(metricsDefinition, config);

        EvaluationStrategy strategy = new DefaultStrategy(metricsDefinition);
        Metric metric = null;
        do {
            metric = strategy.getNextMetric();
            if (metric!= null){
                js.eval(resultScript);
                ret = js.invokeFunction(metric.getName());
                results.put(metric.getName(), new MetricEvaluationResult(metric, ret));
            }
        }
        while (metric!=null);

        return results;
    }


    @Override
    public MetricsDefinition getMetricsDefinition() {
        return null;
    }

    public Object eval(MetricsDefinition metricsDefinition, Metric metric, RenderConfig config) {
        Object ret;
        MetricsSparkJs js = getMetricsSparkJs();
        String resultScript = renderScript(metricsDefinition, config);
        /*try {*/
            //for now am going just to let the rdds to be created at this point
            js.eval(resultScript);
            ret = js.invokeFunction(metric.getName()/*config.getMetricFunctionName()*/);
        /*}
        finally {
            js.stopContext();
        }*/
        return ret;
     }


    @Override
    public void stop() {
        getMetricsSparkJs().stopContext();
    }

    public String renderScript(MetricsDefinition metricsDefinition){
       return this.renderScript(metricsDefinition, config);
    }



}
