package es.kibu.geoapis.metrics;

import es.kibu.geoapis.objectmodel.MetricsDefinition;

import java.io.Serializable;

/**
 * Created by lrodr_000 on 22/08/2016.
 */
public class MetricsMessage implements Serializable{

    MetricsDefinition metricsDefinitionToEvaluate;

    public MetricsMessage(MetricsDefinition metricsDefinitionToEvaluate) {
        this.metricsDefinitionToEvaluate = metricsDefinitionToEvaluate;
    }

    public MetricsMessage() {
    }

    public MetricsDefinition getMetricsDefinitionToEvaluate() {
        return metricsDefinitionToEvaluate;
    }

    public void setMetricsDefinitionToEvaluate(MetricsDefinition metricsDefinitionToEvaluate) {
        this.metricsDefinitionToEvaluate = metricsDefinitionToEvaluate;
    }

    public static MetricsMessage forMetric(MetricsDefinition metricsDefinition) {
        return new MetricsMessage(metricsDefinition);
    }

}
