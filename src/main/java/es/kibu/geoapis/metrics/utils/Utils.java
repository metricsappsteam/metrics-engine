package es.kibu.geoapis.metrics.utils;

import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by lrodr_000 on 21/08/2016.
 */
public class Utils {

    public static String getFileContent(String fileName, ClassLoader classLoader) {
        StringBuilder result = new StringBuilder("");
        //Get file from resources folder
        InputStream file = classLoader.getResourceAsStream(fileName);

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }
            scanner.close();
        }

        return result.toString();
    }

}
