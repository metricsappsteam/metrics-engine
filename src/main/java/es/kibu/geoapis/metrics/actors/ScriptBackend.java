package es.kibu.geoapis.metrics.actors;

import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import es.kibu.geoapis.metrics.App;
import es.kibu.geoapis.metrics.MetricsMessage;
import es.kibu.geoapis.metrics.engine.MetricsEngineFactory;

//#backend
public class ScriptBackend extends UntypedActor {

    LoggingAdapter log = Logging.getLogger(getContext().system(), this);

    boolean fake;
    public ScriptBackend (boolean fake){
        this.fake = fake;
    }

    @Override
    public void onReceive(Object message) {

        if (message instanceof String) {
            Object executionResult;
            if (fake) executionResult = "executed!!";
            else {
                log.debug("Running the analysis : {}", "App.evaluateWorldCount()");
                executionResult = App.evaluateWorldCount();
            }

            log.debug("Result obtained : {}", executionResult);
            getSender().tell(executionResult.toString(), self());

        } else if (message instanceof MetricsMessage){
            MetricsEngineFactory.getMetricEngine().eval(((MetricsMessage) message).getMetricsDefinitionToEvaluate());
        }else {
            unhandled(message);
        }
    }
}
//#backend

