package es.kibu.geoapis.metrics.actors;

import akka.actor.UntypedActor;
import es.kibu.geoapis.metrics.MetricsMessage;

/**
 * Created by lrodr_000 on 22/08/2016.
 */
public class DataProducer extends UntypedActor{

    @Override
    public void onReceive(Object message) throws Exception {
         if (message instanceof MetricsMessage) {

         }
         else {
             unhandled(message);
         }
    }
}
