var DataTypes = require('eclairjs/sql/types').DataTypes;
var DataType = require('eclairjs/sql/types/DataType');
var SQLContext = require('eclairjs/sql/SQLContext');
var sql = require('eclairjs/sql');
var RowFactory = sql.RowFactory;
//var DataFrame = sql.DataFrame;
var SparkConf = require('eclairjs/SparkConf');
var SparkContext = require('eclairjs/SparkContext');
/*var sparkConf = new SparkConf().setAppName("JavaScriptSparkSQL");
 var ctx = new SparkContext(sparkConf);
 var sqlContext = new SQLContext(ctx);*/


function convert(sc, json, parquetFileName){

    var jsonFile = json;// ((typeof args !== "undefined") && (args.length > 2)) ? args[2] :"examples/data/test.json"

    var ctx = sc;
    var sqlContext = new SQLContext(ctx);

    var dataFrame = sqlContext.read().json(jsonFile);

    var path = parquetFileName; //"data.parquet_"+ new Date().getUTCMilliseconds();

    dataFrame.write().parquet(path);

    dataFrame.show(5);

    return path;
}

if (typeof  sc === 'undefined'){
    var SparkContext = require(EclairJS_Globals.NAMESPACE + '/SparkContext');
    var sc = new SparkContext("local[*]", "core examples unit test");
}

var generate = function(jsonFile, parquetFileName) {

    print("parquetFileName: " + parquetFileName);
    print("jsonFile: " + jsonFile);
    return convert(sc, jsonFile, parquetFileName);
}