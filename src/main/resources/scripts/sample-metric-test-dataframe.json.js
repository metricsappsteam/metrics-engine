var DataTypes = require('eclairjs/sql/types').DataTypes;
var DataType = require('eclairjs/sql/types/DataType');
var SQLContext = require('eclairjs/sql/SQLContext');
var MetricsUtils = require('scripts/api/metricsutils.js');
var GeoUtils = require('scripts/api/geoutils.js');
var coreutils = require('scripts/api/coreutils.js');

require("lib/turf.min.js");

coreutils.debugMode = false;
var sql = require('eclairjs/sql');
var RowFactory = sql.RowFactory;

if (typeof  sc === 'undefined'){
    var SparkContext = require(EclairJS_Globals.NAMESPACE + '/SparkContext');
    var sc = new SparkContext("local[4]", "core examples unit test");
}

//Spark context is assumed that the context will be provided by the underlying engine.
var sqlContext = new SQLContext(sc);
var application = {};

print('Spark version: ' + sc.version());

//where the real data comes from?
//var parquetDir = 'generated-data/2016_09_19_18_13_37/';
var parquetDir = 'generated-data/2016_09_19_18_02_19/';
application.movement = sqlContext.read().parquet(parquetDir + 'variable-movement.parquet' );
var movement = application.movement;
print("+---------------------------------+");
print("Data for 'movement': |");
movement.show(60);

/*
var mc = {
    "sessionId": "session-937432643",
    "userId": "user-1630913046",
    "applicationId": "application--1705372261",
};
*/
//|application--1382627754|[37.184175,-3.600781,2012-11-08T05:54:14.000Z]|[session-1298517471,2016-09-19T18:02:19.540+02:00]|1659457122|2012-11-08T05:54:14.000Z|user-762320993 |
var mc = {
    "sessionId": "session-1298517471",
    "userId": "user-762320993",
    "applicationId": "application--1382627754",
};


//some important columns of the dataframe
var movementApplication = movement.col("application");
var movementSessionId = movement.col("session.id");
var movementUser = movement.col("user");
//register the a template with the variable name
movement.registerTempTable("movement");

print('-----------------');
print('Metrics Context:');
print(mc);
print('-----------------');


var session = {};
var user = {};
var userSession = {};

/** session: contains the values for the variables in the current session **/

session.movement = movement.filter(movementSessionId.equalTo(mc.sessionId));

/**
 user: contains the values for the variables for the current user (mc.userId)). Note that the this data does is NOT
 filtered by session.
 **/

user.movement = movement.filter(movementApplication.equalTo(mc.applicationId).and(movementUser.equalTo(mc.userId)));


/**user-session: data of the variables defined filetered by user and session.**/

userSession.movement = movement.filter(movementApplication.equalTo(mc.applicationId).and(movementSessionId.equalTo(mc.sessionId)));


//Scopes will be registered in the following way there will be some default scopes:
var withSteps = /*movement.filter(function (record) { return record.session.id == mc.sessionId && record.steps > 0; })*/ userSession.movement.filter(movement.col('steps').gt(0));

//metrics
var simpleMetric = function (){
    var movementsInSession =  userSession.movement.filter(movement.col('steps').gt(0)); return movementsInSession.count();
};
var stepsStats = function (){
    var movStats =  withSteps.describe(movement.col('steps')); /*var mean = movStats.filter(movStats.col('summary').equalTo('mean')).count();*/ return 1;
};
var simpleMetricScopes = function (){
    return withSteps.count();
};


/*
userSession.movement.show(10);
userSession.movement.describe().show();
*/

/*var locations = withSteps.map(function (row) {
    print(row.get(1));
});*/

var more10e5 = withSteps.filter(withSteps.col('steps').gt(10e8));
print("more10e5: " + more10e5.count());
more10e5.show();

var movStat1 =  withSteps.describe().toRDD().map(function(row) {
    print(row);
    print(row.get(1));
    return row.get(1);/*{"stat:": + row.getString(0), "value": row.getFloat(1)}*/ ;
}
);

movStat1.foreach(function (e) {
    print(e);
});

/*
print('stats1:');
print(movStat1);
print(userSession.movement.dtypes());
*/

var locations = userSession.movement.map(function (row){
    var latlon = MetricsUtils.getAsLatLon(row,  row.fieldIndex("location"));
    return latlon;
});

print('locaitons:' + locations.count());

var movStats =  withSteps.describe();
print('stats:');
movStats.show();

var statsNames = ['mean', 'min', 'max', 'stddev', 'count', 'fail'];

print(statsNames.length);
print(statsNames);
var collected = movStats.collect();

var stats = MetricsUtils.getStats(collected, statsNames);
print("new stats:", JSON.stringify(stats));

for (var i = 0; i< statsNames.length; i++){
    var name = statsNames[i];
    print('--------------------------------------------------------------');
    print('------------------------'+name+'------------------------------');
    print('the "'+  name +'"  is: ' + MetricsUtils.getStats(movStats, name));
    print('(array) the "'+  name +'"  is: ' + MetricsUtils.getStats(collected, name));
    print('--------------------------------------------------------------');

}

//mean distance
/*
sumCount = nums.aggregate((0, 0),
    (lambda acc, value: (acc[0] + value, acc[1] + 1),
    (lambda acc1, acc2: (acc1[0] + acc2[0], acc1[1] + acc2[1]))))
*/


var userSessionLocations = userSession.movement.map(function (row){
    var latlon = MetricsUtils.getAsLatLon(row,  row.fieldIndex("location"));
    var time =  MetricsUtils.getTimeDefault(row);
    var timeStr = MetricsUtils.getTimeStr(row);
    var result = {"latlon": latlon/*.lat,  "lon": latlon.lon*/, "time": time, "iso_time": timeStr};
    return result;
});

var points = userSessionLocations.map(function (location) {
    var latlon = location.latlon;
    var pt = GeoUtils.createPoint(latlon.lat, latlon.lon);
    return pt;
});


var pointsArr = userSessionLocations.map(function (location) {
    var latlon = location.latlon;
    var pts = [];
    pts.push(latlon.lon);
    pts.push(latlon.lat);
    return pts;
});

print('creating all points  -BEGIN');
var allPoints = /*GeoUtils.featureCollectionFromLatLongArray(pointsArr.collect());//*/
    GeoUtils.getFeatureCollection(points.collect());

print('creating all points - END');

print('creating linestring - BEGIN');

var linestring = turf.lineString(pointsArr.collect());
print('creating linestring - END');

print("******************************************************************");
print("-------------------ALL POINTS FC---------------------------------");
print(JSON.stringify(allPoints));
print("-------------------ALL POINTS FC END-----------------------------");


function SpeedData(speed, timeLastTime, distance, lastLocation, count, bbox, lastTimeStr) {
    this.speed = speed;
    this.lastTime = timeLastTime;
    this.distance = distance;
    this.lastLocation = lastLocation;
    this.count = count;
    this.bbox = bbox;
    this.lastTimeStr = lastTimeStr;
}


var distances = userSessionLocations.aggregate(new SpeedData(0,  null, 0, null, 0), function (init, row){

    var latlon = row.latlon;//MetricsUtils.getAsLatLon(row, latlon.fieldIndex("location"));
    if (coreutils.debugMode) {
        print(JSON.stringify(latlon));
    }

    var time =  row.time;//MetricsUtils.getTimeDefault(row);
    var lastLoc = init.lastLocation;
    var lastTimeStr = row.iso_time;

    if (lastLoc == null){
        if (coreutils.debugMode){
            print ('lastloc is null:' /*+ JSON.stringify(lastLoc)*/);
        }
        init.distance = 0;
        init.speed = 0;
    }
    else {

        var distance = GeoUtils.getDistanceLatLonKm(lastLoc, latlon);
        init.distance += distance;
        var diffTime =  Math.abs(time - init.lastTime);

        if (coreutils.debugMode) {
            print('times: init->'+ init.lastTimeStr + ' row->' + lastTimeStr);
            print('time diff: '+ diffTime + ' ms' + ' ' + MetricsUtils.msToSeconds(diffTime)+ 'secs');
            print('distance: ' + distance + ' km');
            print('acc distance: '+ init.distance + ' km');
        }

        var newspeed = distance*1000/(diffTime/1000);
        var speedKMH = MetricsUtils.speedMstoKmH(newspeed);

        if (coreutils.debugMode) {
            print('speed: '+ newspeed+ ' m/s');
            print('speed: '+ speedKMH+ ' km/h');
        }

        var newCount = 1;
        init.speed = ((init.speed * init.count) +  newspeed)/ init.count + newCount;

        if (init.bbox == null){
            var pt = GeoUtils.createPoint(latlon.lat, latlon.lon);
            var pt1 = GeoUtils.createPoint(lastLoc.lat, lastLoc.lon);
            var bbox = turf.bbox(turf.featureCollection([pt, pt1]));
            if (coreutils.debugMode){
                print ("calculated bbox:" + JSON.stringify(bbox));
            }
            init.bbox = GeoUtils.bboxToCollection(bbox);
        }
        else {
            var bboxPolygon = init.bbox;
            var pt = GeoUtils.createPoint(latlon.lat, latlon.lon);;
            init.bbox = GeoUtils.addToBbox(bboxPolygon, pt);
        }

    }
    init.lastLocation = latlon;
    init.lastTime = time;
    init.lastTimeStr = lastTimeStr;
    init.count += 1;

    if (coreutils.debugMode){
        print("return:" + JSON.stringify(init));
    }
    return init;

}, function(init1,   init2) {
    var result = {};

    if ( coreutils.debugMode) {
        print ("reducing...") ;
        if (init1 != null) { print("init1: "+ JSON.stringify(init1));}
        if (init2 != null) { print("init2: "+JSON.stringify(init2));}

        if (init1 == null || init2 == null) print("nulls found init1:" + init1 + " init2:"+ init2);
    }

    if (init1!= null && init2!= null) {

        if (coreutils.debugMode) {
            print("calculating....");
        }

        result.speed = ((init1.speed * init1.count) +  (init2.speed * init2.count))/ (init1.count + init2.count);
        result.lastTime = init1.lastTime;
        result.distance = init1.distance + init2.distance;
        result.lastLocation = init1.lastLocation;
        result.count = init1.count + init2.count;

        if (init1.bbox!=null && init2.bbox!= null){
            if (coreutils.debugMode) {
                print("bbox1:" + JSON.stringify(init1.bbox));
                print("bbox2:" + JSON.stringify(init2.bbox));
            }
            var union = GeoUtils.collectionUnion(init1.bbox, init2.bbox);
            if (coreutils.debugMode) {
                print("union:" + JSON.stringify(union));
            }
            result.bbox = GeoUtils.bboxToCollection(turf.bbox(union));
        }
        else if (init1.bbox!=null && init2.bbox==null) {

            if (coreutils.debugMode) {
                print("bbox1:" + JSON.stringify(init1.bbox));
            }
            result.bbox = init1.bbox;
        }
        else if (init1.bbox == null && init2.bbox!=null) {
            if ( coreutils.debugMode){
                print("bbox2:" + JSON.stringify(init2.bbox));
            }
            result.bbox = init2.bbox;
        }
        else {
            if (coreutils.debugMode){
                print("setting a bbox to null as nono of them has value");
            }
            result.bbox = null;
        }
    }

    return result;
});


var bboxAndPoints = GeoUtils.collectionUnion(allPoints,  distances.bbox);
var bboxAndPointsLS = GeoUtils.collectionUnion(turf.featureCollection(linestring), distances.bbox);

if (coreutils.debugMode) {
    print(">>>>UNION:");
    print(JSON.stringify(bboxAndPoints));
    print(">>UNION END<<");

    print(">>>>UNION LS:");
    print(JSON.stringify(bboxAndPointsLS));
    print(">>UNION LS END<<");


}

print("Result(distances): " + JSON.stringify(distances));
