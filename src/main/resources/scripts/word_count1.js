/*
 * this is an example that is supposed to show how to use this chained scripts
 */

/**/
/*
var SparkContext = require(EclairJS_Globals.NAMESPACE + '/SparkContext');
var sparkContext = new SparkContext("local[*]", "core examples unit test");
*/

//require("./lib/turf.min.js");

function run(sparkContext) {
    var Tuple2 = require('eclairjs/Tuple2');

    var file = ((typeof args !== "undefined") && (args.length > 1)) ? args[1] : "/user/hdfs/insurance.csv";



    var rdd = sparkContext.textFile(file).cache();


    var rdd2 = rdd.flatMap(function (sentence) {
        return sentence.split(" ");
    });

    print(rdd2.count());
    var rdd3 = rdd2.filter(function (word) {
        return word.trim().length > 0;
    });

    var rdd4 = rdd3.mapToPair(function (word, Tuple2) {
        return new Tuple2(word, 1);
    }, [Tuple2]);

    var rdd5 = rdd4.reduceByKey(function (a, b) {
        return a + b;
    });

    var rdd6 = rdd5.mapToPair(function (tuple, Tuple2) {
        return new Tuple2(tuple._2() + 0.0, tuple._1());
    }, [Tuple2])

    var rdd7 = rdd6.sortByKey(false);
    return JSON.stringify(rdd7.take(10));


}

function runSimple(sparkContext) {

    var Tuple2 = require('eclairjs/Tuple2');

    var file = ((typeof arguments !== "undefined") && (arguments.length > 1)) ? arguments[1] : "samplefile.txt";

   var rdd = sparkContext.textFile(file).cache();
/*    var arr = ['a b c', 'd e f'];

    print("arr length:" + arr.length);
    for (var el in arr) {
        print(arr[el]);
    }

    var rdd = sparkContext.parallelize(arr).cache();*/

    var rdd2 = rdd.flatMap(function (sentence) {
        var split = sentence.split(" ");
        return split;
    });

    print("rdd2 count:" + rdd2.count());
    print("rdd2 collect:" + rdd2.collect());

    var rdd3 = rdd2.filter(function (word) {
        print((typeof  word) + ':' + word);
        return word.trim().length > 0;
    });

    var rdd4 = rdd3.mapToPair(function (word, Tuple2) {
        return new Tuple2(word, 1);
    }, [Tuple2]);

    var rdd5 = rdd4.reduceByKey(function (a, b) {
        return a + b;
    });

    var rdd6 = rdd5.mapToPair(function (tuple, Tuple2) {
        return new Tuple2(tuple._2() + 0.0, tuple._1());
    }, [Tuple2])

    var rdd7 = rdd6.sortByKey(false);
    return rdd7.take(10);

}

/*
 check if SparkContext is defined, if it is we are being run from Unit Test
 */

/*if (typeof sparkContext === 'undefined') {
    var SparkConf = require('eclairjs/SparkConf');
    var SparkContext = require('eclairjs/SparkContext');
    var conf = new SparkConf().setAppName("JavaScript word count");
    var sparkContext = new SparkContext(conf);
}*/

if (typeof sc === 'undefined' || sc == null){

    var SparkContext = require(EclairJS_Globals.NAMESPACE + '/SparkContext');
    sc = new SparkContext("local[*]", "core examples unit test");

}


var WordCount = function() {
    return run(sc);
}

var WordCount1 = function() {
    print("arguments:" + arguments.length);

    var i = 0;
    for(var arg in arguments) {
        print("arg["+ i +"]: " + arguments [arg]);
        i++;
    }

    return runSimple(sc, arguments[0]);
}


