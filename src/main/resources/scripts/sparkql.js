var DataTypes = require('eclairjs/sql/types').DataTypes;
var DataType = require('eclairjs/sql/types/DataType');
var SQLContext = require('eclairjs/sql/SQLContext');
var sql = require('eclairjs/sql');
var RowFactory = sql.RowFactory;
//var DataFrame = sql.DataFrame;
var SparkConf = require('eclairjs/SparkConf');
var SparkContext = require('eclairjs/SparkContext');
/*var sparkConf = new SparkConf().setAppName("JavaScriptSparkSQL");
var ctx = new SparkContext(sparkConf);
var sqlContext = new SQLContext(ctx);*/


function runSimple(sc, file, json){

    var filename = file;//((typeof args !== "undefined") && (args.length > 1)) ? args[1] : "examples/data/people.txt";
    var jsonFile = json;// ((typeof args !== "undefined") && (args.length > 2)) ? args[2] :"examples/data/test.json"

    var ctx = sc;
    var sqlContext = new SQLContext(ctx);
// Load a text file and convert each line to a JavaScript Object.
    var people = ctx.textFile(filename).map(function(line) {
        var parts = line.split(",");
        return person = {
            name: parts[0],
            age: parseInt(parts[1].trim())
        };
    });

//Generate the schema
    var fields = [];
    fields.push(DataTypes.createStructField("name", DataTypes.StringType, true));
    fields.push(DataTypes.createStructField("age", DataTypes.IntegerType, true));
    var schema = DataTypes.createStructType(fields);
// Convert records of the RDD (people) to Rows.
    var rowRDD = people.map(function(person, RowFactory){
        return RowFactory.create([person.name, person.age]);
    },[RowFactory]);


//Apply the schema to the RDD.
    var peopleDataFrame = sqlContext.createDataFrame(rowRDD, schema);

// Register the DataFrame as a table.
    peopleDataFrame.registerTempTable("people");

// SQL can be run over RDDs that have been registered as tables.
    var results = sqlContext.sql("SELECT name FROM people");

//The results of SQL queries are DataFrames and support all the normal RDD operations.
//The columns of a row in the result can be accessed by ordinal.
    var names = results.toRDD().map(function(row) {
        return "Name: " + row.getString(0);
    });

    print("names = " + names.take(10));
    var dataFrame = sqlContext.read().json(jsonFile);
    var col = dataFrame.col("first")
    var gd = dataFrame.groupBy(col);
    var df2 = gd.count();

    //even writing to parquet. Yes, that works
    //dataFrame.write().parquet("people.parquet");

    df2.show();
    df2.count();

}


if (typeof  sc === 'undefined'){

    var SparkContext = require(EclairJS_Globals.NAMESPACE + '/SparkContext');
    var sc = new SparkContext("local[*]", "core examples unit test");

}

var sparkQL = function() {
    print("arguments:" + arguments.length);

    var i = 0;
    for(var arg in arguments) {
        print("arg["+ i +"]: " + arguments [arg]);
        i++;
    }

    return runSimple(sc, arguments[0], arguments[1]);
}