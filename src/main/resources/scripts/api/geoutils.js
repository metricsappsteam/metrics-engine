/**
 * Created by lrodr_000 on 26/09/2016.
 */
(function () {
    require("lib/turf.min.js");
    var coreutils = require('scripts/api/coreutils.js');
    var mu = require('scripts/api/metricsutils.js');


    function bboxToCollection(bbox) {
        var bboxPolygon = turf.bboxPolygon(bbox);
        if (coreutils.debugMode) {
            print('bboxToCollection' + JSON.stringify(bboxPolygon));
        }
        var resultFeatures = bboxPolygon;
        var result = {
            "type": "FeatureCollection",
            "features": resultFeatures
        };
        return result;
    }

    function addToBbox(initialFeatureCollection, point) {
        if (isFeatureCollection(initialFeatureCollection)) {

            if (coreutils.debugMode) {
                print('BEGIN addToBbox -----------------------------------------------------------------------');
                print('addToBbox' + JSON.stringify(initialFeatureCollection));
            }

            var features = [];
            features.push(point);
            features = features.concat(initialFeatureCollection.features);

            if (coreutils.debugMode) {
                print("features:" + JSON.stringify(features));
            }
            var fc = turf.featureCollection(features);

            if (coreutils.debugMode) {
                print(JSON.stringify(fc));
            }

            var bbox = turf.bbox(fc);

            if (coreutils.debugMode) {
                print('END addToBbox -----------------------------------------------------------------------');
            }
            return bboxToCollection(bbox);
        }
        else {
            if (coreutils.debugMode) {
                print('This method can not handle this geo object: ' + initialFeatureCollection.type)
            }
            throw 'This method can not handle this geo object.' + JSON.stringify(initialFeatureCollection);
        }
    }

    function isFeatureCollection(fc) {
        return fc.type == 'FeatureCollection';
    }

    function collectionUnion(fc1, fc2) {
        if (isFeatureCollection(fc1) && isFeatureCollection(fc2)) {
            var features = [];
            features = features.concat(fc1.features);
            features = features.concat(fc2.features);
            var newfc = turf.featureCollection(features);
            return newfc;
        }
        else {
            throw "This method can not handle this type of geoobject";
        }
    }


    function getDistanceLatLonKm(latlon1, latlon2) {
        return getDistanceFromLatLonInKm(latlon1.lat, latlon1.lon, latlon2.lat, latlon2.lon);
    }

//distance
    function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = mu.deg2rad(lat2 - lat1);  // mu.deg2rad below
        var dLon = mu.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(mu.deg2rad(lat1)) * Math.cos(mu.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }


    function getLatLon(/*lat, lon*/) {
        if (coreutils.debugMode) {
            print('arguments getLatLon:' + JSON.stringify(arguments));
        }

        var tempLat;
        var tempLon;
        if (arguments.length == 2) {
            if (coreutils.debugMode) {
                print("separate lat and lon");
            }
            //a
            tempLat = arguments[0]; // lat;
            tempLon = arguments[1];//lon;
        }
        else {
            if (typeof  arguments[0] == 'object') {
                if (coreutils.debugMode) {
                    print("an object with lat and lon");
                }

                var obj = arguments[0];
                tempLat = coreutils.getPropOrNull(obj, 'latitude');
                tempLon = coreutils.getPropOrNull(obj, 'longitude');

                if (tempLon == null) {
                    tempLon = coreutils.getPropOrNull(obj, 'lon');

                }
                if (tempLat == null) {
                    tempLat = coreutils.getPropOrNull(obj, 'lat');
                }
            }
            else if (coreutils.isArray(arguments[0])) {
                //read it as se sequence of lon lat
                if (coreutils.debugMode) {
                    print("an array of elements");
                }
                var arrLonLat = arguments[0];
                tempLon = arrLonLat[0];
                tempLat = arrLonLat[1];
            }
        }
        return {"lat": tempLat, "lon": tempLon};
    }

    function createPoint(/*lat, lon*/) {

        if (coreutils.debugMode) {
            coreutils.printd('createPoint:' + JSON.stringify(arguments));
        }

        var latLon = getLatLon.apply(null, arguments);

        if (coreutils.debugMode){
            coreutils.printd('createPoint after apply:' + JSON.stringify(latLon) + 'lat:' + latLon.lat + 'lon:' + latLon.lon);
        }

        return turf.point([latLon.lon, latLon.lat]);
    }

    function featureCollectionFromLatLongArray(latLongArr) {

        if (coreutils.debugMode) {
            print('latLongArr:' + JSON.stringify(latLongArr));
        }

        if (!coreutils.isArray(latLongArr)) throw 'latLongArr parameter must be an array';
        coreutils.checkElemsNotNull(latLongArr);

        var pts = [];
        for (var i = 0; i < latLongArr.length; i++) {
            var latlon = latLongArr[i];
            var pt = GeoUtils.createPoint(latlon[1], latlon[0]/*latlon.lat, latlon.lon*/);
            pts.push(pt);
        }

        if (pts.length > 0) {
            return getFeatureCollection(pts);
        }
        else throw 'The must have at  least one  point.';
    }

    function getFeatureCollection(features) {
        var featureCollection = turf.featureCollection(features);
        return featureCollection;
    }

//exported functions
    module.exports = {
        bboxToCollection: bboxToCollection,
        addToBbox: addToBbox,
        collectionUnion: collectionUnion,
        isFeatureCollection: isFeatureCollection,
        getDistanceFromLatLonInKm: getDistanceFromLatLonInKm,
        getDistanceLatLonKm: getDistanceLatLonKm,
        createPoint: createPoint,
        featureCollectionFromLatLongArray: featureCollectionFromLatLongArray,
        getFeatureCollection: getFeatureCollection
    }
})();
