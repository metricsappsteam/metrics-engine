/**
 * Created by lrodr_000 on 26/09/2016.
 */

(function () {

    var debugMode;

    function printIfDebug(message){
        print(message);
    }

    function isUndefinedOrNull(obj) {
        return (typeof obj == 'undefined') || (obj == null);
    }

    /*function isArray(x) {
        return x.constructor.toString().indexOf("Array") > -1;
    }*/

    function isArray(obj) {
        if (debugMode) {
            print("obj:" + obj);
        }
        if (typeof obj == 'undefined') {
            return false;
        }
        return (obj.constructor === Array);
    }


    function getPropOrNull(obj, propName) {
        (typeof obj[propName] != 'undefined') ? obj[propName] : null;
    }

    function isObject(obj) {
        return typeof  obj == 'object';
    }

    function checkElemsNotNull(arr) {
        if (isArray(arr)) {
            arr.forEach(function (item) {
                if (item == null) throw 'Elements must not be null';
            })
            /*for (var i = 0; i < arr.length; i++) {
             if (arr[i] == null) {
             throw 'Elements in the array must not be null';
             }
             }*/
        }
    }


    module.exports = {
        isUndefinedOrNull: isUndefinedOrNull,
        isArray: isArray,
        getPropOrNull: getPropOrNull,
        isObject: isObject,
        debugMode: debugMode,
        checkElemsNotNull: checkElemsNotNull,
        printIfDebug: printIfDebug,
        printd:printIfDebug
    }

})();

