(function () {

    var coreutils = require('scripts/api/coreutils.js');


    function getStatValue(rowsArray, statsName) {
        var statValue = NaN;
        for (var i = 0; i < rowsArray.length; i++) {
            var row = rowsArray[i];
            if (row.get(0) == statsName) {
                statValue = row.get(1);
                return statValue;
            }
        }
        return statValue;
    }

    function getStats(rowsDescribeResult, statsName) {
        var statValue = NaN;
        if (coreutils.debugMode) {
            print("rows:" + rowsDescribeResult);
            print("statsname:" + statsName);
        }

        if (coreutils.isUndefinedOrNull(rowsDescribeResult)) {
            throw "rowsDescribeResult can not be null nor undefined";
        }
        if (coreutils.isUndefinedOrNull(statsName)) {
            throw "statsName can not be null nor undefined";
        }

        var rowsArray = rowsDescribeResult;
        if (!coreutils.isArray(rowsArray)) {
            if (coreutils.debugMode) {
                print('is not array so collecting..');
            }
            rowsArray = rowsDescribeResult.collect();
        }
        else {
            if (coreutils.debugMode) {
                print('is array so directly accessing ..');
            }
        }

        if (coreutils.isArray(statsName)) {
            var valObj = {};
            for (var i = 0; i < statsName.length; i++) {
                var name = statsName[i];
                var partialValue = getStatValue(rowsArray, name);
                valObj[name] = partialValue;
            }
            statValue = valObj;
        }
        else {
            statValue = getStatValue(rowsArray, statsName);
        }
        return statValue;
    }

    function getMean(rowDataFrame) {
        return getStat(rowDataFrame, "mean");
    }

    function getStdDev(rowDataFrame) {
        return getStat(rowDataFrame, "stddev");
    }

    function getCount(rowDataFrame) {
        return getStat(rowDataFrame, "count");
    }

    function getMax(rowDataFrame) {
        return getStat(rowDataFrame, "max");
    }

    function getMin(rowDataFrame) {
        return getStat(rowDataFrame, "min");
    }

    function deg2rad(deg) {
        return deg * (Math.PI / 180);
    }

    function getAsLatLon(row, fieldIndex) {
        var latlon = row.getStruct(fieldIndex);
        return {"lat": latlon.get(latlon.fieldIndex("latitude")), "lon": latlon.get(latlon.fieldIndex("longitude"))};
    }

    function getTime(row, fieldName) {
        var value = row.get(row.fieldIndex(fieldName));
        return Date.parse(value);
    }

    function getTimeStr(row, fieldName) {
        var field = arguments.length > 1 ? fieldName : 'time';
        var value = row.get(row.fieldIndex(field));
        return value;
    }


    function getTimeDefault(row) {
        return getTime(row, 'time');
    }

    function msToSeconds(ms) {
        return ms / 1000;
    }

    function msToHr(ms) {
        return (ms / 1000) / (60 * 60);
    }

    function speedMstoKmH(ms) {
        return (ms / 1000) * (60 * 60);
    }


    module.exports = {
        deg2rad: deg2rad,
        getStats: getStats,
        getMax: getMax,
        getMin: getMin,
        getMean: getMean,
        getCount: getCount,
        getStdDev: getStdDev,
        getAsLatLon: getAsLatLon,
        getTime: getTime,
        getTimeDefault: getTimeDefault,
        getTimeStr: getTimeStr,
        msToSeconds: msToSeconds,
        msToHr: msToHr,
        speedMstoKmH: speedMstoKmH
    };

})();


