var DataTypes = require('eclairjs/sql/types').DataTypes;
var DataType = require('eclairjs/sql/types/DataType');
var SQLContext = require('eclairjs/sql/SQLContext');
var sql = require('eclairjs/sql');
var RowFactory = sql.RowFactory;
//var DataFrame = sql.DataFrame;
var SparkConf = require('eclairjs/SparkConf');
var SparkContext = require('eclairjs/SparkContext');
/*var sparkConf = new SparkConf().setAppName("JavaScriptSparkSQL");
var ctx = new SparkContext(sparkConf);
var sqlContext = new SQLContext(ctx);*/


function runSimple(sc, json){

    var jsonFile = json;// ((typeof args !== "undefined") && (args.length > 2)) ? args[2] :"examples/data/test.json"

    var ctx = sc;
    var sqlContext = new SQLContext(ctx);

    var dataFrame = sqlContext.read().json(jsonFile);
    var col = dataFrame.col("application")
    var gd = dataFrame.groupBy(col);
    var df2 = gd.count();

    //even writing to parquet. Yes, that works
    var path = "generated-data/data.parquet_"+ new Date().getUTCMilliseconds();
    dataFrame.write().parquet(path);

    var dp = sqlContext.read().parquet(path);

    dp.show(5);

    df2.show();
    df2.count();

    return path;
}


if (typeof  sc === 'undefined'){

    var SparkContext = require(EclairJS_Globals.NAMESPACE + '/SparkContext');
    var sc = new SparkContext("local[*]", "core examples unit test");

}

var sparkQL = function() {
    print("arguments:" + arguments.length);

    var i = 0;
    for(var arg in arguments) {
        print("arg["+ i +"]: " + arguments [arg]);
        i++;
    }

    return runSimple(sc, arguments[0], arguments[1]);
}