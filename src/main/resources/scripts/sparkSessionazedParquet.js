var DataTypes = require('eclairjs/sql/types').DataTypes;
var DataType = require('eclairjs/sql/types/DataType');
var SQLContext = require('eclairjs/sql/SQLContext');
var sql = require('eclairjs/sql');
var RowFactory = sql.RowFactory;
//var DataFrame = sql.DataFrame;
var SparkConf = require('eclairjs/SparkConf');
var SparkContext = require('eclairjs/SparkContext');
/*var sparkConf = new SparkConf().setAppName("JavaScriptSparkSQL");
var ctx = new SparkContext(sparkConf);
var sqlContext = new SQLContext(ctx);*/


function runSimple(sc, jsonFile, parquetFile){

    var sqlContext = new SQLContext(sc);

    var dataFrame = sqlContext.read().parquet(parquetFile);
    dataFrame.show(5);

    dataFrame.registerTempTable("parquetFile");

    var sessionsId200 = sqlContext.sql("SELECT session.id as session FROM parquetFile WHERE session.id LIKE '%200%'");

    print(sessionsId200.count());
    //sessionsId200.map(function (t) {"session id: " + t[0]}).collect().foreach(print);

    var col = sessionsId200.col("session")
    var gd = sessionsId200.groupBy(col);

    var grouped = gd.count();

    grouped.show();

}



if (typeof  sc === 'undefined'){

    var SparkContext = require(EclairJS_Globals.NAMESPACE + '/SparkContext');
    var sc = new SparkContext("local[*]", "core examples unit test");

}

var sparkQL = function() {
    print("arguments:" + arguments.length);

    var i = 0;
    for(var arg in arguments) {
        print("arg["+ i +"]: " + arguments [arg]);
        i++;
    }

    return runSimple(sc, arguments[0], arguments[1]);
}